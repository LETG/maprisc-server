# BACKEND MAPRISC - API DJANGO

## Présentation

Dans le cadre du développement de la plateforme MAPRISC pour l'OR2C, ce dépôt concerne le serveur de traitement et l'API qui lui est adossée.
Le serveur repose sur la pile de technologies suivante :
- Docker pour la conteneurisation des services
- Django pour le framework de développement et GeoDjango pour les aspects SIG
- Django-Rest-Framework pour les fonctionnalités spécifiques à l'API
- Celery et Redis pour la gestion des tâches aynschrones, Flower pour le monitoring
- Postgres/PostGIS pour le SGBD

## Usage

### Configuration

Le fichier `production.env` doit contenir les valeurs d'identifiants adaptées à l'instance déployée et utilisées pour le fichier `docker-compose.yml`. Actuellement utilisé uniquement pour les paramètres Postgres.

Le fichier `api/.env` contient les différentes variables d'environnement. Lors de l'installation, ce fichier doit être créé à partir du fichier d'exemple `.env-example` présent dans le dépôt git.
Ce fichier permet de spécifier les paramètres spécifiques à l'instance déployée, à savoir :
  * `MAPRISC_BACKEND_SECRET_KEY` nécessitant la création d'une clé secrète via la commande : `base64 /dev/urandom | head -c50` dont le résultat doit être copié dans le fichier `.env`.
  * `ALLOWED_HOSTS` ainsi que `CORS_WHITELIST` permettant de spécifier les URLs ou IPs autorisées à communiquer avec l'API (i.e. le nom de domaine ou l'IP pour les versions de développement)
  * `MAPRISC_BACKEND_USE_SENTRY` et `MAPRISC_BACKEND_SENTRY_DSN` pour la configuration Sentry (si utilisée).

Certains paramètres sont spécifiques aux modules complémentaires installés. Par exemple, pour la réinitialisation de mot passe (voir https://github.com/anexia-it/django-rest-passwordreset pour plus détails) :
- `DJANGO_REST_MULTITOKENAUTH_RESET_TOKEN_EXPIRY_TIME` (par défaut `24`) définit en heure la durée de validité du token (et donc du lien de réinitialisation) envoyé par mail
- `DJANGO_REST_PASSWORDRESET_NO_INFORMATION_LEAKAGE` (par défaut `False`) permet de spécifier la réponse HTTP en cas d'adresse mail incorrecte. Cela peut permettre d'éviter une fuite de données (récupération d'adresses exploitables par force brute), mais empêche l'utilisateur d'être alerté en cas de mauvaise adresse renseignée.
- **Non utilisé** `DJANGO_REST_MULTITOKENAUTH_REQUIRE_USABLE_PASSWORD` (par défaut `True`) permet d'autoriser la réinitialisation de mot de passe pour un utilisateur n'ayant pas de mot de passe valide (au sens de Django)

### Images docker

L'image `server` repose sur un fichier Dockerfile basé sur une image contenant Python et GDAL. Les instructions consistent à installer les libs nécessaires et à installer les dépendances sous formes de paquets pip.

__NOTE__: Attention, l'installation des dépendances se base sur le fichier `requirements.txt`, il faut donc le mettre à jour lors de l'ajout de paquets pip (soit directement en éditant le fichier, soit via la commande `pip freeze > requirements.txt` si les paquets sont installés localement dans un environnement virtuel Python). Penser également à supprimer le paquet `pkg-resources` du fichier requirements qui est ajouté par `pip freeze` et qui génère des erreurs à la compilation de l'image docker.

Une fois le fichier `requirements.txt` mis à jour, il faut reconstruire l'image avec `docker-compose build server`.

### Installation

Pré-requis:
- docker
- docker-compose

Pour installer l'application :
1. cloner le dépôt : `git clone`
2. renseigner les fichiers de configuration, cf. section `Configuration` plus haut
3. construire le conteneur `server` : `sudo docker-compose build server`
4. appliquer les migrations au niveau de la base de données (i.e. générer les tables SQL conformément aux modèles) : `docker exec -it container_id python manage.py migrate`. Ou bien exécuter cette commande à partir du conteneur lui-même (voir section "Éxécuter les migrations" ci-dessous).
5. Créer un super utilisateur : `docker exec -it container_id python manage.py createsuperuser`. Si cette étape est omise, il faudra ensuite modifier l'utilisateur créé via l'interface directement via Postgres.
 
### Démarrer la séquence de services

Les services démarrent via la composition docker avec la commande suivante : `docker-compose up` pour une version de développement et `docker-compose -f docker-compose.yml -f docker-compose.prod.yml up` pour un déploiement en production. Ajouter l'option `-d` pour un démarrage en tâche de fond.

__Note__: Le service `server` ne doit pas démarrer pas tant que la connexion avec Postgres n'a pu être établie, d'où la propriété `command` spécifique.

Pour arrêter les services : `docker-compose stop`.

### Mise à jour

- Récupérer les dernières mises à jour: `git pull`
- Reconstruire le conteneur server (à partir des mises à jour des dépendances dans requirements.txt): `docker-compose build server`
- (Optionnel) : appliquer les migrations pour la base de données, cf. section `Installation` plus haut
- Démarrer la séquence de services : `docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d` par exemple pour la prod
- Se connecter au conteneur (cf. ci-dessous)
- Éxécuter les migrations (cf. ci-dessous)

### Se connecter au conteneur

__Note/Todo__: le fichier makefile ne semble pas permettre de lancer une commande directement via docker. Pour pallier ce problème, il faut se connecter au conteneur via une session shell.

`docker container exec -it maprisc_server_server_1 /bin/bash`

### Exécuter les migrations

__Remarque__: Django utilise un système de scripts de migration lorsque des modifications sont apportées aux modèles (ORM) des différentes applications installées.

Une fois connecté au conteneur, exécuter la commande :
- `python manage.py makemigrations common accounts dune`. Il semble qu'il faut préciser les différentes applications pour lesquelles générer les fichiers de migrations
- puis, `python manage.py migrate` pour appliquer les migrations

L'exécution des migrations doit se faire chaque fois que les modèles ont subi des changements. À chaque mise à jour d'un dépôt local (via `git pull`) avec des changements sur les modèles, il faut donc appliquer les migrations.

### Lancer les tests

Une fois connecté au conteneur (cf. section ci-dessus), exécuter la commande : `pytest`

### Connexion à l'interface Flower

L'outil Flower est utilisé pour le suivi des tâches asynchrones Celery associées au gestionnaire de file d'attente Redis. Flower expose une API permettant de connaître la liste des tâches en cours, terminées ou qui ont échouées.
L'interface n'est pas accessible publiquement car il n'existe pas de moyen d'assurer une authentification des utilisateurs. Par conséquent, Django sert d'intermédiaire pour interroger l'API de manière authentifiée, et l'interface n'est accessible que depuis le réseau interne de l'Université.

Pour se connecter à l'interface (via VPN ou directement depuis le réseau interne), il faut se rendre à l'adresse : https://<ip-vm-maprisc>:5555 où l'IP correspond à l'IP interne de la VM hébergeant le service. **Attention**: le port 5555 doit être autorisé au niveau de la configuration réseau sur l'IAAS.

### (optionnel) Django Admin URL

Le serveur n'a pas vocation à servir des pages web, mais uniquement à exposer des routes via l'API. Cependant, l'interface d'administration reste accessible pour faciliter la gestion des utilisateurs.

URL d'accès : `http://0.0.0.0:8000/admin/`

### Documentation de l'API

L'API repose sur les spécifications OpenAPI et la documentation est générée automatiquement via Swagger.

- URL de la documentation : `http://<ip-vm-maprisc>:8000/_platform/docs/v1/swagger`
- la documentation précise les différentes routes et méthodes associées. L'URL de base pour l'API est : `http://<ip-vm-maprisc>:8000/api/v1/`

## Auteurs

- Benjamin Hervy
- Martin Juigner
- Cyril Pulvin