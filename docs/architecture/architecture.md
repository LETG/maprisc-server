# Architecture du backend Django

La documentation ci-dessous n'a pas vocation à être exhaustive mais à fournir les éléments de compréhension nécessaire à une prise en main facilitée du projet.

## Organisation générale des fichiers

L'organisation des fichiers suit l'arborescence Django suivante :
- api/
    * maprisc_backend/
        - apps/
            * accounts/
            * common/
            * dune/
        - fixtures/
        - settings/
        - asgi.py
        - celery.py
        - loggers.py
        - urls.py
        - wsgi.py
- docker/
- docs/
- out/
- README.md
- requirements.txt
- docker-compose.yml
- docker-compose.prod.yml

Le contenu du dossier `/api` est copié dans le dossier `/app` du conteneur docker `maprisc_backend`.
Le dossier `maprisc_backend` contient la structure globale d'un projet Django avec les différentes applications dans `apps`, et les fichiers de configuration principaux (routes principales et configuration swagger pour la documentation API, variables Django, etc.).

Une application est créée pour chaque module MAPRISC afin de bien décloisonner les logiques.

L'architecture des applications Django est définie dans les sections suivantes.

## Structure des applications Django

Les applications du projet Django sont placées dans le dossier `/maprisc_backend/apps`.
Le projet est actuellement (au 27/04/21) composé de 3 applications:
- `common` pour l'ensemble des fonctionnalités communes aux autres applications (modèles de base, enregistrement des actions et statistiques)
- `accounts` pour la gestion des utilisateurs : authentification classique et par JSON Web Token
- `dune` correspond à l'application relative au module de traitement des profils de plage

La structure générique d'une application est la suivante :
- `api/` : contient le code relatif à la définition des vues et sérialiseurs pour l'exposition en tant qu'API REST
- `migrations/` : contient les fichiers python Django permettant de rejouer les modifications du modèle de la base de données. Ces fichiers sont générés automatiquement
- `models/` : contient la déclaration des modèles relatifs à l'application
- `services/` : contient d'éventuels services en lien avec une application Django classique
- `tests/` : contient les tests relatifs à l'application
- `utils/` : contient d'éventuels fichiers utilitaires spécifiques pour le traitement des données

**Note importante** : la documentation relative à l'ensemble des routes de l'API est exposée via l'outil swagger qui génère les spécifications de chaque route automatiquement. L'URL de cette documentation est la suivante : `https://maprisc-or2c.univ-nantes.fr/_platform/docs/v1/swagger`

### Application common

### Application accounts

L'application dune concerne la gestion des comptes utilisateur. Consulter la documentation swagger pour plus détails sur les routes autorisées.

- `api/`
    * `v1/`
        - `serializers/`
            * `login.py`
            * `password.py`
            * `recaptcha.py`
            * `registration.py`
            * `user_profile.py`
        - `views/`
            * `login.py`
            * `password.py`
            * `recaptcha.py` : vue spécifique permettant à l'API publique de communiquer avec l'API Google pour accéder au service Recaptcha (nécessite un compte google et une déclaration du service pour obtenir une clé définie comme variable d'environnement)
            * `registration.py`
            * `summary.py` : vue personnalisée pour la récupération de statistiques d'usage de la plateforme
            * `user_profile.py` : vue dédiée à la récupération des tokens JWT pour l'authentification ainsi qu'à la modification des utilisateurs (administrateur uniquement)
        - `urls.py`
    * `authentication.py`
    * `permissions.py`
- `migrations/`
- `models/`
    * `user_account.py`
- `tests/`
    * `test_models/`
    * `test_api/`
    * `test_serializers/`
    * `test_services/`

### Application dune

L'application dune concerne le module de traitement des profils de plage. Consulter la documentation swagger pour plus détails sur les routes autorisées.

- `api/`
    * `v1/`
        - `serializers/`
            * `dune_profile.py`
        - `views/`
            * `flower.py` : vue spécifique permettant à l'API publique d'agir comme un intermédiaire auprès de l'API Flower pour le monitoring de la file d'attente Celery. L'API publique s'assure de l'authentification pour appeller le service docker Flower qui lui n'est pas sécurisé.
            * `summary.py` : vue personnalisée pour obtenir des statistiques d'usage de l'application dune.
            * `transect.py` : vues dédiées à la gestion des profils au niveau de l'entité Transect (avec dates imbriquées ou non)
            * `datedprofile.py` : vues dédiées à la gestion des données datées liées à un transect (ajout, modification, suppression d'une ou plusieurs dates)
        - `tasks.py` : tâches de traitement placées dans la file d'attente gérée par Celery. Les tâches sont appelées depuis les vues (i.e. les actions associées aux routes d'envoi de données)
        - `urls.py`
    * `authentication.py`
    * `permissions.py`
- `migrations/`
- `models/`
    * `dune_profile.py`
- `tests/`
    * `test_utils/`
    * `test_api/`
- `utils/`
    * `dune.py` : programme contenant les fonctions principales pour le traitement des données de profils de plage. Le programme inclut la définition des filtres, l'algorithme de détection des pieds et crêtes de dune, et le calcul des bilans sédimentaires.