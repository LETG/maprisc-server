.PHONY: makemigrations
makemigrations:
	sudo docker-compose run --rm server python manage.py makemigrations


.PHONY: migrate
migrate:
	sudo docker-compose run --rm server python manage.py migrate


.PHONY: dshell
dshell:
	sudo docker-compose run --rm server python manage.py shell


.PHONY: flake
flake:
	sudo docker-compose run --rm server flake8 .


.PHONY: psql
psql:
	sudo docker-compose run --rm postgres psql --host=postgres --dbname=maprisc_backend --username=postgres
