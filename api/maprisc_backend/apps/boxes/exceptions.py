from django.core.exceptions import ValidationError


class InvalidBaseLineError(ValidationError):

    pass

class InvalidFilesError(ValidationError):

    pass