from django.apps import AppConfig


class BoxesConfig(AppConfig):
    name = 'maprisc_backend.apps.boxes'
