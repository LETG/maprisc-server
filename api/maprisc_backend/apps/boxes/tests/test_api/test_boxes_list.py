from django.urls import reverse
from django.core.files.base import ContentFile
from django.test.client import MULTIPART_CONTENT, encode_multipart, BOUNDARY
from rest_framework import status

import pytest
import os
import json
from collections import OrderedDict
from maprisc_backend.apps.boxes.tests.api_client import boxes_api_client

from maprisc_backend.apps.boxes.models import File, Boxes
from maprisc_backend.apps.common.models import UserAction

THIS_DIR = os.path.dirname(os.path.abspath(__file__))

from rest_framework.test import APIClient

class TestSimpleApp:
    def test_one(self):
        x = "my simple app test"
        assert 'simple app' in x

@pytest.mark.django_db
def test_get_boxes_api_list_empty(boxes_api_client):
    """ Random user has no associated boxes"""

    url = reverse("api-v1-boxes:boxes-list")
    client = boxes_api_client()
    response = client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()["results"]) == 0

@pytest.mark.django_db
def test_get_boxes_api_list_unauthorized(unauthorized_api_client):
    """ Unauthorized request should fail """

    url = reverse("api-v1-boxes:boxes-list")
    response = unauthorized_api_client.get(url)

    assert response.status_code == status.HTTP_401_UNAUTHORIZED

@pytest.mark.django_db
@pytest.mark.skip(reason="To be fixed")
def ignore_test_create_boxe_success(api_client):
    assert Boxes.objects.count() == 0

    test_ldbfile_path = os.path.join(THIS_DIR, 'essaiCut.zip')
    files=[
        ('baseline_file',('essaiCut.zip',open(test_ldbfile_path,'rb'),'application/zip'))
    ]
    data = {
        'name': 'test 1',
        'creation_date': '14/11/2022',
        'toCut': 'False',
        'cutting_polygon': 'null',
        'toSimplify': 'False',
        'simplification_tolerance': '0',
        'transects_distance': '100',
        'offset_to_parallels': '50',
        'toSmooth': 'True',
        'smoothing_value': '3',
        'baseline_file': files
    }

    payload={'name': 'test 2',
    'creation_date': '15/11/2022',
    'toCut': 'False',
    'cutting_polygon': 'null',
    'toSimplify': 'False',
    'simplification_tolerance': '0',
    'transects_distance': '200',
    'offset_to_parallels': '50',
    'toSmooth': 'False',
    'smoothing_value': '3'}

    files=[
        ('baseline_file',('essaiCut.zip',open(test_ldbfile_path,'rb'),'application/zip'))
    ]

    client = api_client()
    response = client.post(reverse("api-v1-boxes:boxes-list"), payload)
    

    #client = api_client()
    #client=APIClient()
    #response = client.post(reverse("api-v1-boxes:boxes-list"), data)
    #response = client.post(reverse("api-v1-boxes:boxes-list"), {'image': tmp_file}, format='multipart')
    #response = client.post(reverse("api-v1-boxes:boxes-list"), data, format='multipart')

    #Create dict content from geojson file
    test_file_path = os.path.join(THIS_DIR, 'response.json')
    with open(test_file_path) as geojson_file:
        content = json.load(geojson_file)
        data = [{
            'date': "2022-11-15",
            'uncertainty': 5,
            'geojson': content,
        }]  # nosec


    assert response.status_code == status.HTTP_201_CREATED
    assert Boxes.objects.count() == 1
    assert response.data['points_count'] == 362 #data["362"]
    assert response.data['boxes_count'] == 361 #data["361"]
    assert response.data['name'] == "test 1"
    #assert response.data['profile_axis']['coordinates'] == data["profile_axis"]
    #assert response.data['dated_profiles'] == OrderedDict([('type', 'FeatureCollection'), ('features', [])])
    assert response.data['result_boxes'] == "{\"type\": \"FeatureCollection\", \"features\": [{\"id\": \"0\", \"type\": \"Feature\", \"properties\": {\"area_ratio\": 6.430127541534603e-14, \"shape\": 1.0000000000006508, \"box_id\": 0, \"cat_id\": 0, \"rl_segment_ratio\": 1.0}, \"geometry\": {\"type\": \"Polygon\", \"coordinates\": [[[292260.1434026618, 6638646.814909845], [292170.4565973324, 6638602.585090154], [292348.6030420423, 6638467.441299186], [292260.1434026618, 6638646.814909845], [292258.91623671283, 6638423.211479495], [292348.6030420423, 6638467.441299186], [292258.91623671283, 6638423.211479495], [292170.4565973324, 6638602.585090154], [292260.1434026618, 6638646.814909845]]]}}, {\"id\": \"1\", \"type\": \"Feature\", \"properties\": {\"area_ratio\": 1.0000000000043847, \"shape\": 1.0000000000016305, \"box_id\": 1, \"cat_id\": 0, \"rl_segment_ratio\": 1.0}, \"geometry\": {\"type\": \"Polygon\", \"coordinates\": [[[292348.6030420423, 6638467.441299186], [292258.91623671283, 6638423.211479495], [292348.6030420423, 6638467.441299186], [292437.0626814228, 6638288.0676885275], [292347.3758760934, 6638243.837868837], [292437.0626814228, 6638288.0676885275], [292347.3758760934, 6638243.837868837], [292258.91623671283, 6638423.211479495], [292348.6030420423, 6638467.441299186]]]}}, {\"id\": \"2\", \"type\": \"Feature\", \"properties\": {\"area_ratio\": 0.9998028019907284, \"shape\": 1.2054934402868545, \"box_id\": 2, \"cat_id\": 0, \"rl_segment_ratio\": 1.46}, \"geometry\": {\"type\": \"Polygon\", \"coordinates\": [[[292437.0626814228, 6638288.0676885275], [292347.3758760934, 6638243.837868837], [292437.0626814228, 6638288.0676885275], [292439.60755676124, 6638282.907329732], [292457.8193960274, 6638250.128792065], [292468.9769088477, 6638234.276046761], [292479.7651183972, 6638223.871045422], [292504.61699349893, 6638208.944437667], [292538.98650438915, 6638193.734281393], [292556.21172245295, 6638187.099860237], [292520.2697524398, 6638093.782258512], [292556.21172245295, 6638187.099860237], [292520.2697524398, 6638093.782258512], [292501.9013149924, 6638100.856999137], [292499.6378122452, 6638101.793097702], [292461.30411224393, 6638118.757597704], [292455.79417998187, 6638121.617439178], [292423.4024799823, 6638141.072639176], [292418.7247177493, 6638144.255645826], [292414.43651753315, 6638147.946760801], [292396.1234175379, 6638165.6093608], [292392.8630287753, 6638169.067798243], [292389.9457579421, 6638172.82019152], [292374.51445794274, 6638194.745191521], [292371.6955082875, 6638199.239383745], [292351.5988082823, 6638235.410383745], [292350.4623973337, 6638237.579190155], [292347.3758760934, 6638243.837868837], [292437.0626814228, 6638288.0676885275]]]}}]}"
    assert response.data['transect_lines'] == "????"


@pytest.mark.django_db
def test_post_single_baseline_success(boxes_api_client):
    """
    Input file should contain single part baseline.
    If so, server should return HTTP 202 code.
    """

    test_ldbfile_path = os.path.join(THIS_DIR, 'essaiCut_singlePart.zip')

    with open(test_ldbfile_path, "rb") as f:
        file_data = ContentFile(f.read(), test_ldbfile_path)

        payload = {
            'name': 'test 2',
            'creation_date': '15/11/2022',
            'toCut': 'False',
            'cutting_polygon': 'null',
            'toSimplify': 'False',
            'simplification_tolerance': '0',
            'transects_distance': '200',
            'offset_to_parallels': '50',
            'toSmooth': 'False',
            'smoothing_value': '3',
            'baseline_file': file_data,
        }
        client = boxes_api_client()
        response = client.post(
            reverse("api-v1-boxes:boxes-list"),
            data=encode_multipart(boundary=BOUNDARY, data=payload),
            content_type=MULTIPART_CONTENT,
        )

        assert response.status_code == status.HTTP_202_ACCEPTED

@pytest.mark.django_db
def test_post_multipart_baseline_bad_request(boxes_api_client):
    """
    Input file should contain single part baseline.
    If multipart geometry is detected, server should return HTTP 400 error.
    """

    test_ldbfile_path = os.path.join(THIS_DIR, 'essaiCut_multiPart.zip')

    with open(test_ldbfile_path, "rb") as f:
        file_data = ContentFile(f.read(), test_ldbfile_path)

        payload={
            'name': 'test 2',
            'creation_date': '15/11/2022',
            'toCut': 'False',
            'cutting_polygon': 'null',
            'toSimplify': 'False',
            'simplification_tolerance': '0',
            'transects_distance': '200',
            'offset_to_parallels': '50',
            'toSmooth': 'False',
            'smoothing_value': '3',
            'baseline_file': file_data,
        }
        client = boxes_api_client()
        response = client.post(
            reverse("api-v1-boxes:boxes-list"),
            data=encode_multipart(boundary=BOUNDARY, data=payload),
            content_type=MULTIPART_CONTENT,
        )

        assert response.status_code == status.HTTP_400_BAD_REQUEST