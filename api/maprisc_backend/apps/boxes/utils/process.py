import os
import sys
import time
import logging

import matplotlib.pyplot as plt
import numpy as np
import geopandas as gpd

from django.contrib.gis import geos as geos
from django.conf import settings

from scipy import interpolate
from scipy.ndimage import gaussian_filter1d

from shapely.ops import split
from shapely.ops import snap
from shapely.ops import polygonize
from shapely.geometry import Point, MultiPoint, LineString, MultiLineString, Polygon
from shapely import wkt
from shapely.ops import nearest_points
from shapely.affinity import scale

from maprisc_backend.apps.boxes.models.boxes import Boxes, TransectLine



def importFileAsGeometry(filename: str) -> gpd.GeoDataFrame:
    """ 
    Import a base line from a file and return the geoDataFrame

    Parameters
    ---------------
    filename : a file, zip format of an Esri shape file (shp)
    """

    readbaseline = gpd.read_file(filename)   
    logging.debug("- import file: ", filename)

    return readbaseline


def simplifyLine(line: LineString, tolerance: float) -> LineString:
    """ 
    Simplify a line through shapely.simplify method
    All points in the simplified object will be within the tolerance distance of the original geometry
    Returns a simplified representation of the geometric object.

    Parameter
    ---------------
    line : input line to simplify. Shapely Linestring object
    tolerance : tolerance
    """
    
    logging.debug('- simplifyLine')
    simplified_line = line.simplify(tolerance)

    return simplified_line


def smoothGaussian(line: LineString, sigma: int) -> LineString:
    """ 
    Smooth a line by Gaussian filter and return a smoothed LineString

    Parameters
    ---------------
    sigma : int
        Standard deviation parameter of the gaussian
    """
    logging.debug('- smoothGaussian')
    ctr =np.array(line)
    x=ctr[:,0]
    y=ctr[:,1]
    l=len(x)  
    t = np.linspace(0, 1, l)
    t2 = np.linspace(0, 1, l)

    x2 = np.interp(t2, t, x)
    y2 = np.interp(t2, t, y)

    x3 = gaussian_filter1d(x2, sigma)
    y3 = gaussian_filter1d(y2, sigma)

    x4 = np.interp(t, t2, x3)
    y4 = np.interp(t, t2, y3)
    nb=len(x4)
    plist=[]
    for i in range(nb):
        p = Point(x4[i],y4[i])
        plist.append(p)

    # add boundariess
    first, last = line.boundary
    lineout = LineString([first] + [point for point in plist] + [last])


    return lineout


def smoothLine(line, r_lissage):
    """
    Smooth a line. The method associates splprep and splev scipy methods. 
    Returns a LineString

    1. splprep : 
    Find the B-spline representation of an N-dimensional curve.

    Given a list of N rank-1 arrays, `x`, which represent a curve in
    N-dimensional space parametrized by `u`, find a smooth approximating
    spline curve g(`u`).

    usage => tck2,u2 = interpolate.splprep([x,y],k=3,s=0)

    splprep Parameters
    ----------
    x : array_like
        A list of sample vector arrays representing the curve.
    k : int, optional
        Degree of the spline. Cubic splines are recommended.
        Even values of `k` should be avoided especially with a small s-value.
        ``1 <= k <= 5``, default is 3.
    s : float, optional
        A smoothing condition.  The amount of smoothness is determined by
        satisfying the conditions: ``sum((w * (y - g))**2,axis=0) <= s``,
        where g(x) is the smoothed interpolation of (x,y).  The user can
        use `s` to control the trade-off between closeness and smoothness
        of fit.  Larger `s` means more smoothing while smaller values of `s`
        indicate less smoothing. Recommended values of `s` depend on the
        weights, w.  If the weights represent the inverse of the
        standard-deviation of y, then a good `s` value should be found in
        the range ``(m-sqrt(2*m),m+sqrt(2*m))``, where m is the number of
        data points in x, y, and w.
    
    splprep Returns
    -------
    tck : tuple
        (t,c,k) a tuple containing the vector of knots, the B-spline
        coefficients, and the degree of the spline.
    u : array
        An array of the values of the parameter.

    2. splev :
    Evaluate a B-spline or its derivatives.
    Given the knots and coefficients of a B-spline representation, evaluate
    the value of the smoothing polynomial and its derivatives.

    usage => out3 = interpolate.splev(u3,tck3)

    *************************************************************************

    splev Parameters
    ----------
    u3 : array_like
        An array of points at which to return the value of the smoothed
        spline or its derivatives.  If `tck` was returned from `splprep`,
        then the parameter values, u should be given.
    tck3 : 3-tuple or a BSpline object
        If a tuple, then it should be a sequence of length 3 returned by
        `splrep` or `splprep` containing the knots, coefficients, and degree
        of the spline. (Also see Notes.)

    splev Returns
    -------
    out3 : ndarray or list of ndarrays
        An array of values representing the spline function evaluated at
        the points in `u3`.  If `tck` was returned from `splprep`, then this
        is a list of arrays representing the curve in N-dimensional space.

    """

    logging.debug('- smoothLine BSpline')
    ctr =np.array(line)
    x=ctr[:,0]
    y=ctr[:,1]

    l=len(x)  
    nb = int(l/r_lissage)
    logging.debug('  -> nb of summits on base line :' + str(len(x)) + " -> keep for smoothing: " + str(nb))
    tck2,u2 = interpolate.splprep([x,y],k=3,s=0)
    u2=np.linspace(0,2,num=nb,endpoint=True)
    out2 = interpolate.splev(u2,tck2)

    ti=np.linspace(0,1,l-2,endpoint=True)
    ti=np.append([0,0,0],ti)
    ti=np.append(ti,[1,1,1])

    tck3=[ti,[out2[0], out2[1]],3]
    u3=np.linspace(0,1,(max(l*2,70)),endpoint=True)
    out3 = interpolate.splev(u3,tck3)

    plist=[]
    for i in range(nb):
        p = Point(out3[0][i],out3[1][i])
        plist.append(p)

    lineout = LineString([point for point in plist])
    return lineout


def process_parallel_line(line, offset):
    """ 
    Returns 2 LineString geometries at a distance from the object on its right and its left side.

    Parameters
    ----------------
    line : the base line
    offset : distance to the base line
    """

    logging.debug('- process parallel lines')
    resolution=16
    join_style=1
    mitre_limit=10

    offset_l = line.parallel_offset(offset, 'left', resolution, join_style, mitre_limit) # lissage dans le calcul de l'offset: param join_style=1 (round), resolution et mitre_limit
    offset_r = line.parallel_offset(offset, 'right', resolution, join_style, mitre_limit)   

    return [offset_l, offset_r]


def process_points(line, ecart):
    """
    Returns array of points on line, each separated by ecart distance

    Parameters
    ---------------
    line : the base line
    ecart : distance between each point
    """

    logging.debug('- process points on line')
    distances = np.arange(0, int(line.length), ecart)
    points = [line.interpolate(distance) for distance in distances] + [line.boundary[1]]
    logging.debug("ecart: " + str(ecart) + " => nb of extracted points : " + str(len(points)))
    return points

    
def create_transect(point, offset_1, offset_2, direction):
    """
    create transects V1
    Creates Shapely LineString object between two parallel offset lines, passing through a point

    Parameters
    - point: Shapely Point object which is the base point from which to draw the transect
    - offset_1, offset_2: Two parallel offset lines
    - direction: Boolean that specifies the ordering of point coordinates in the output LineString
    """
    _, nearest_pt = nearest_points(point, offset_1)
    transect_ref = LineString([point, nearest_pt])
    # Scale the line to intersect with offset line
    transect = scale(transect_ref, xfact=10, yfact=10, origin=nearest_pt)
    opposite_pt = transect.intersection(offset_2)
    # Check if intersection returns MultiPoint (occurs when line intersects with offset multiple times, e.g. in curvy shapes)
    if type(opposite_pt) == MultiPoint:
        # Get point with minimal distance among all intersections
        _, opposite_pt = nearest_points(point, opposite_pt)
    if type(opposite_pt) == Point:
        transect_points = [nearest_pt, opposite_pt]
        if direction == True:
            transect = LineString(transect_points)
        else:
            transect = LineString(transect_points[::-1])
        return (transect_ref, transect)
    else:
        #log ("Could not create transect: no intersection found with opposite parallel offset line")
        return (transect_ref, None)


def check_multiple_intersection(line, transect):
    """
    Checks wether the transect intersects more than once with the original shoreline.
    """

    return type(transect.intersection) == MultiPoint

def process_transects(points, offset_l, offset_r):
    logging.debug('- process transects V1 (transect passe par point central)')
    """
    compute transects for each point based on nearest points on parallel offset lines
    
    Parameters
    ---------------
    points : points on the base line spaced of distance 'ECART' (see process_points
    offset_l, offset_r : parallel lines to baseline (see process_parallel_line)

    """
    transects = {'geometry': []}
    transects_base = {'geometry': []}
    for point in points:
        transect_right_ref, transect_right_offset = create_transect(point, offset_r, offset_l, direction=False)
        transect_left_ref, transect_left_offset = create_transect(point, offset_l, offset_r, direction=True)

        if not transect_right_offset and not transect_left_offset:
            continue    
        elif not transect_right_offset:
            transect = transect_left_offset
            transect_ref = transect_left_ref
        elif not transect_left_offset:
            transect = transect_right_offset
            transect_ref = transect_right_ref
        else:
            if transect_right_offset.length > transect_left_offset.length:
                transect = transect_left_offset
                transect_ref = transect_left_ref
            else:
                transect = transect_right_offset
                transect_ref = transect_right_ref

        # If the transect make multiple intersections with original shoreline, skip it
        # This helps to overcome the problem of extremely narrowed shapes
        new_line = LineString(points)
        if check_multiple_intersection(new_line, transect):
            pass
        else:
            transects['geometry'].append(transect)
            transects_base['geometry'].append(transect_ref)
    
    logging.debug(" -> Nombre de transects V1 crées : {}".format(len(transects["geometry"]))) 
                        
    return (transects, transects_base)


def create_transect_v2(point, offset_1, offset_2):
    """
    Creates Shapely LineString object between two parallel offset lines, passing through a point.

    Parameters
    ---------------
    - point: Shapely Point object which is the base point from which to draw the transect
    - offset_1, offset_2: Two parallel offset lines

    """
    _, nearest_ptl = nearest_points(point, offset_1)
    _, nearest_ptr = nearest_points(point, offset_2)
    transect_ref = LineString([point, nearest_ptl])
    transect = LineString([nearest_ptl, nearest_ptr])
    return (transect_ref, transect)
    

def process_transects_v2(points, offset_l, offset_r, transectlines):
    """
        create transects V2
        compute transects for each point based on nearest points on parallel offset lines
        but the transects doest not have to intersect then point on base line
    """
    index = 0
    transects = {'geometry': []}
    transects_base = {'geometry': []}
    previous_transect_geom = None
    nbRejets = 0
    for point in points:
        transect_ref, transect = create_transect_v2(point, offset_l, offset_r)

        # If the transect make multiple intersections with original shoreline, skip it
        # This helps to overcome the problem of extremely narrowed shapes
        new_line = LineString(points)
        if check_multiple_intersection(new_line, transect):
            nbRejets +=1
            pass
        else:
            if transect != previous_transect_geom:
                # check if the new transect is not exactly the same as previous
                transects['geometry'].append(transect)
                transects_base['geometry'].append(transect_ref)
                previous_transect_geom = transect

                # ************************
                # ATTENTION : transect is a SHAPELY.LineString => need to convert shapely to django.contrib.gis.geos.LineString
                # ************************
                trline = {'transectline_id': {}, 'geometry': {} }
                trline['transectline_id'] = index 
                x,y = transect.coords.xy
                DATA = list(zip(x,y))
                trline['geometry'] = geos.LineString(DATA, srid=2154)
                transectlines.append(trline)
                index+=1
            else:
                nbRejets +=1
                pass
    
    logging.debug(" -> Nombre de transects V2 crées : {}".format(len(transects["geometry"]))) 
    logging.debug(" -> Nombre de rejets V2 (pour cause de multiples intersections avec la ligne lissée ou geometrie identique au transect précédent)  :" + str(nbRejets)) 
    
    return (transects, transects_base)


def check_create_transect_split2(transects, offsetR, offsetL, ecart):
    """
    CREATION DE TRANSECTS INTERMEDIAIRES (pour améliorer répartition)
    => parcoure les transects et en ajoute là où la distance entre 2 transects est importante 
  
    """
    logging.debug('- check and create_transect_split')
    previous_transect=None
    newTransects = {'geometry': []}
    newIdTransect=0
    nbNewTransects = 0
    nbRejets = 0
    for tr_id,transect in enumerate(transects["geometry"]):
        newIdTransect = tr_id

        # try to create a new transect at the mid distance between transect and previous
        if previous_transect:
            firstTransect, lastTransect = transect.boundary
            firstPreviousTransect, lastPreviousTransect = previous_transect.boundary
            line1 = LineString([firstTransect, firstPreviousTransect])
            center1 = line1.centroid
            line2 = LineString([lastTransect, lastPreviousTransect])
            center2 = line2.centroid
         
            newTr_part1 = LineString([center1, center2])
            # Scaling the line newTr_part to intersect with each offset line
            tr1 = scale(newTr_part1, xfact=3, yfact=3, origin=center1)
            centroidL = tr1.intersection(offsetL)
             # Check if intersection returns MultiPoint (occurs when line intersects with offset multiple times, e.g. in curvy shapes)
            if type(centroidL) == MultiPoint:
                # Get point with minimal distance among all intersections
                _, centroidL = nearest_points(center1, centroidL)
                if type(centroidL) != Point:
                    centroidL = None
            
            newTr_part2 = LineString([center2, center1])
            tr2 = scale(newTr_part2, xfact=3, yfact=3, origin=center2)
            centroidR = tr2.intersection(offsetR)
             # Check if intersection returns MultiPoint (occurs when line intersects with offset multiple times, e.g. in curvy shapes)
            if type(centroidR) == MultiPoint:
                # Get point with minimal distance among all intersections
                _, centroidR = nearest_points(center2, centroidR)
                if type(centroidR) != Point:
                    centroidR = None
                
            #("type of centroid: ", type(centroidR), type(centroidL))
            if centroidL and centroidR and type(centroidR)==Point and type(centroidL)==Point: 
                newTransect = LineString([centroidR, centroidL])
                #if int(line1.length) > (1.5*ecart) or int(line2.length) > (1.5*ecart):
                if line1.length > ecart or line2.length > ecart:
                    #("length between transects important => adding transect")
                    newTransects['geometry'].append(newTransect)
                    nbNewTransects+=1
                else:
                    #("lenght between transects is weak, give up")
                    nbRejets+=1
            else:
                    #("invalid points")
                    nbRejets+=1

        previous_transect = transect
    
    logging.debug(" -> Nombre de transects intermediaires crées : {}".format(len(newTransects["geometry"]))) 
    logging.debug(" -> Nombre de rejets :"  +str(nbRejets)) 
    
    return newTransects

def process_boxes(processResult: Boxes, transects, offset_left, offset_right, boxes, offset, ecart, line):
    """
    compute boxes by creating polygons based on previously created transects
    requires prepared geometries
    
    Parameters
    ---------------
    processResult : Boxes object to complete 
    transects : prepared transects spaced with ecart
    offset_left, offset_right : parallel lines to the baseline and at offset distance
    boxes : geometries created by this process
    offset, ecart : main parameters of the full process 
    line : the baseline
    centroids : geometries created with computed centroids of boxes

    """
    logging.debug('- process boxes (tient compte de la géométrie de la ligne //')
    previous_transect = None
    index = 0
    nbFailsR=0
    nbFailsL=0
    listSegOffsetR=[]
    listSegOffsetL=[]
    pointsL=[]
    pointsR=[]

    Rpeaks=0
    Lpeaks=0

    segment_offset_left = LineString()
    segment_offset_right=  LineString()
    remaining_offset_left = LineString()
    remaining_offset_right = LineString()

    numero = 0
    listTransects=[]
    lines1=[]
    lines2=[]
    rejectedShapes=0
    previous_transect = None   
    remaining_offset_left =  offset_left
    remaining_offset_right = offset_right

    nbTransects=len(transects["geometry"])-1
    for transect_id, transect in enumerate(transects["geometry"]):
        if previous_transect:
            # boundaries of the transect
            bound1, bound2 = transect.boundary 
            bound1Prev, bound2Prev = previous_transect.boundary 

            # ******************* transect trprev **************************************************
            if offset_left.distance(bound1Prev) < 1e-8 :
            # CASE 1: bound1Prev on offset LEFT
                # a. LEFT
                offset_left = snap(offset_left,bound1Prev, 0.000000001)
                pointL_trprev = bound1Prev
                if numero==nbTransects:
                    offset_left = snap(offset_left,bound1Prev, 0.000000001)
                    pointL_trprev = nearest_points(offset_left, bound1Prev)[0]

                # b. RIGHT
                offset_right = snap(offset_right,bound2, 0.000000001)
                pointR_trprev = bound2Prev
                if numero==nbTransects:
                    offset_right = snap(offset_right,bound2Prev, 0.000000001)
                    pointR_trprev = nearest_points(offset_right, bound2Prev)[0]

            else:
            # CASE 2: bound1Prev on offset RIGHT
                logging.debug(" - reverse boundaries prev")
                 # a. LEFT
                offset_left = snap(offset_left,bound2Prev, 0.000000001)
                pointL_tr = bound2Prev
                if numero==nbTransects:
                    offset_left = snap(offset_left,bound2Prev, 0.000000001)
                    pointL_trprev = nearest_points(offset_left, bound2Prev)[0]

                # b. RIGHT
                offset_right = snap(offset_right,bound1Prev, 0.000000001)
                pointR_trprev = bound1Prev
                split1 = split(offset_right,pointR_trprev)
                if numero==nbTransects:
                    offset_right = snap(offset_right,bound1Prev, 0.000000001)
                    pointR_trprev = nearest_points(offset_right, bound1Prev)[0]
            # ---------- END transect trprev ------------------------------------------
                

            # # ----------  transect tr -----------------------------------------------
            if offset_left.distance(bound1) < 1e-8 :
            # CASE 1: bound 1 on offset LEFT

                # a. LEFT
                offset_left = snap(offset_left,bound1, 0.000000001)
                pointL_tr = bound1
                if numero==nbTransects:
                    offset_left = snap(offset_left,bound1, 0.1)
                    pointL_tr = nearest_points(offset_left, bound1)[0]

                # b. RIGHT
                offset_right = snap(offset_right,bound2, 0.000000001)
                pointR_tr = bound2
                if numero==nbTransects:
                    offset_right = snap(offset_right,bound2, 0.000000001)
                    pointR_tr = nearest_points(offset_right, bound2)[0]

            else:
            # CASE 2: bound 1 on offset RIGHT
                logging.debug(" - reverse boundaries tr")
                 # a. LEFT
                offset_left = snap(offset_left,bound2, 0.000000001)
                pointL_tr = bound2
                if numero==nbTransects:
                    offset_left = snap(offset_left,bound2, 0.000000001)
                    pointL_tr = nearest_points(offset_left, bound2)[0]

                # b. RIGHT
                offset_right = snap(offset_right,bound1, 0.000000001)
                pointR_tr = bound1
                if numero==nbTransects:
                    offset_right = snap(offset_right,bound1, 0.000000001)
                    pointR_tr = nearest_points(offset_right, bound1)[0]
            # ---------- end for transect tr -----------------------------------------
          
            # ----------------- checks -----------------
            if type(pointL_tr)!=Point or type(pointR_tr)!=Point or type(pointL_trprev)!=Point or type(pointR_trprev)!=Point :
                logging.debug("Unexpected type for boundary operation")
                pass

            if not offset_right.contains(pointR_tr) and not offset_right.contains(pointL_tr):
                logging.debug("l'offset right ne contient ni point L ni point R => Anomalie de calcul")

            if not offset_left.contains(pointL_tr) and not offset_left.contains(pointR_tr):
                logging.debug("l'offset LEFT ne contient ni point L ni point R => Anomalie de calcul")

            # Compute LEFT segment
            segment_offset_left = None
            split1 = split(offset_left,pointL_tr)

            if len(split1) > 1 and numero<nbTransects:
                part1=split1[0] 
                split2 = split(part1,pointL_trprev)
                if len(split2) > 1:
                    segment_offset_left = split2[1] 
                    listSegOffsetL.append(segment_offset_left)
                else :
                    # point at limit: start of offset
                    if numero==1:
                        segment_offset_left = split2[0]  
                    else:
                        logging.debug("L-segment not splitable => Lpeak  (transect n°" + str(numero) + ")")
                        segment_offset_left = LineString([pointL_tr, pointL_trprev])
                        Lpeaks+=1

                    listSegOffsetL.append(segment_offset_left)

            else:
                logging.debug("L-Split1 function does not return the right number of segments (transect n°" + str(numero) + ")")
                nbFailsL+=1
            if segment_offset_left:
                pass
            else:
                logging.debug("segment L is null")   

            # point at limit: end of offset
            if numero==nbTransects:
                split2 = split(offset_left,pointL_trprev)
                if len(split2) > 1:
                    segment_offset_left = split2[1]
                    listSegOffsetL.append(segment_offset_left)


            # compute RIGHT segment
            segment_offset_right = None
            split1 = split(offset_right,pointR_tr)
            if len(split1) > 1 and numero<nbTransects:
                part1=split1[1] 
                split2 = split(part1,pointR_trprev)
                if len(split2) > 1:
                    segment_offset_right = split2[0]
                    listSegOffsetR.append(segment_offset_right)
                else :
                    # point at limit: start of offset
                    if numero==1:
                        segment_offset_right = part1
                    else:
                        segment_offset_right = LineString([pointR_tr, pointR_trprev])
                        Rpeaks+=1
                        logging.debug("R-segment not splitable => Rpeak (transect n°" + str(numero) + ")")

                    listSegOffsetR.append(segment_offset_right)
                    
            else:
                logging.debug("R-Split1 function does not return the right number of segments (transect n°" + str(numero) + ")")
                nbFailsR+=1
            if segment_offset_right:
                pass
            else:
                logging.debug("segment R est null")   

            # point at limit: end of offset
            if numero==nbTransects:
                split2 = split(offset_right,pointR_trprev)
                if len(split2) > 0:
                    segment_offset_right = split2[0] # ok 
                    listSegOffsetR.append(segment_offset_right)

            if numero==1:
                pointsL.append(pointL_trprev)
                pointsR.append(pointR_trprev)
            pointsL.append(pointL_tr)
            pointsR.append(pointR_tr)

            if segment_offset_right and segment_offset_left:
               
                # Method 1: create box as polygonize lines
                #multi_line = MultiLineString([previous_transect, segment_offset_left, transect, segment_offset_right])
                #lines = [previous_transect, segment_offset_left, transect, segment_offset_right]
                #polygonization = list(polygonize(multi_line))
                #box=polygonization[0]
                # Méthod 2: create box from coords                
                pointsList = list(previous_transect.coords) + list(segment_offset_left.coords)+ list(transect.coords)[::-1] + list(segment_offset_right.coords)
                                
                box = Polygon([[p[0], p[1]] for p in pointsList])
                
                centroidBox = box.centroid
                d = line.distance(centroidBox)
                if d<99999999:
                    distCentroidToBaseLine = round(d,2)
                else: 
                    distCentroidToBaseLine = 99999999

                ptLine1 = line.intersection(previous_transect)
                ptLine2 = line.intersection(transect)
                lw = LineString([ptLine1, ptLine2])
                box_width = round(lw.length,2)

                processedBox = {'area_ratio': {}, 'shape': {}, 'geometry': {}, 'box_id':{}, 'cat_id':{}, 'rl_segment_ratio':{}, 'distCentroidToBaseLine':{}, 'centroid_geometry':{}, 'width':{}}
                processedBox['distCentroidToBaseLine'] = distCentroidToBaseLine
                processedBox['centroid_geometry'] = geos.Point(centroidBox.x, centroidBox.y)
                processedBox['width'] = box_width

                # Statistic information
                # AREA RATIO : compute surface ratio from ideal box surface
                ideal_box_surface = 2*offset*ecart
                #boxes['area_ratio'].append(box.area / ideal_box_surface)
                processedBox['area_ratio']= round(box.area / ideal_box_surface,2)
                
                # Compute shape ratio between seaside and landside
                box_splitted = split(box, line)
                # Must check that box has been splitted in two. Otherwise, this is due to some weird box shape
                if(len(list(box_splitted)) == 2):
                    max_area = max(box_splitted[0].area, box_splitted[1].area)
                    min_area = min(box_splitted[0].area, box_splitted[1].area)
                    processedBox['shape']=round(max_area/min_area,2)
                else:
                    processedBox['shape']= -9999 #nodata
                    rejectedShapes = rejectedShapes +1
                
                # RIGHT-LEFT segments comparison
                if segment_offset_right.length >0 and segment_offset_left.length >0:
                    RLsegmentRatio=round((segment_offset_right.length/segment_offset_left.length),2)
                else: 
                    RLsegmentRatio=9999
                processedBox['rl_segment_ratio']=RLsegmentRatio 

                '''
                unknown need:
                nbMagnetPeaks=0
                for t in listTransects:
                    #("transect type:", type(transect))
                    #("t type:", type(t))
                    i = t.intersection(transect)
                    if type(i)==
                    cross=t.intersects(transect) and type(i)==Point and i!=transect.boundary[0] and i!=transect.boundary[1]
                    if cross: 
                        nbIntersectingTrans+=1
                '''
                processedBox['box_id']=index
                processedBox['cat_id']=0

                #processedBox['geometry']= geos.Polygon( ((0, 0), (0, 1), (1, 1), (0, 0)) )
                #boxProcess.geom = geos.Polygon( ((0, 0), (0, 1), (1, 1), (0, 0)) )
                #processedBox['geometry']= geos.Polygon(geos.LinearRing( [[p[0], p[1]] for p in pointsList], srid=2154))
                points = [[p[0], p[1]] for p in pointsList]
                points.append([pointsList[0][0], pointsList[0][1]])
                
                poly = geos.Polygon(geos.LinearRing(points))
                poly = poly.buffer(0)
                processedBox['geometry']= poly #geos.Polygon(box)

                boxes.append(processedBox)
                index+=1
            else:
                logging.debug("segments not available => no box created")
                        
        previous_transect = transect
        listTransects.append(transect)

        numero+=1

    logging.debug(" -> ------- Statistiques création de boites -------------")
    logging.debug("points crees => L:" + str(len(pointsL)) + " R: " + str(len(pointsR)))
    logging.info(f" -> Nombre de boites créées : {len(boxes)}") 
    #logging.debug.debug(" -> Nombre de segment Left en échec: {}".nbFailsL)
    
    # peaks
    logging.debug("nb de sommets R multiples (aimant à transects): " + str(Rpeaks) + " - nb de sommets L multiples: " + str(Lpeaks))
    
    processResult.points_count = len(pointsL)
    processResult.boxes_count = len(boxes)
    processResult.right_fails = nbFailsR
    processResult.left_fails = nbFailsL
    processResult.magnetic_Rpeaks = Rpeaks
    processResult.magnetic_Lpeaks = Lpeaks
    processResult.rejectedShapes = rejectedShapes
    
    return (pointsL, pointsR, listSegOffsetL, listSegOffsetR, lines1, lines2)


def processToBoxes(processParams, basepath, baselinefile):
    """
    Main method for computing boxes
    called from the view

    Parameters
    ---------------
    processParams : model instance to store process parameters
    baselinefile : file containing the base line to import - format = zipped shapefile

    """
    processResult = Boxes()

    start_time = time.time()
    FICHIER_LIGNE_BASE = baselinefile
    ECART = processParams['transects_distance']
    OFFSET = processParams['offset_to_parallels']
    SIMPLIFY = processParams['toSimplify']
    SMOOTHLINE = processParams['toSmooth']
    REDUCTION_LISSAGE = processParams['smoothing_value']
    TOLERANCE =  processParams['simplification_tolerance']

    logging.debug("-----------------------------------")
    logging.debug("Paramétrage du calcul")
    """logging.debug("- fichier ligne de base: " + FICHIER_LIGNE_BASE)
    logging.debug("- simplification: " + str(SIMPLIFY))
    logging.debug("- tolerance (simplification et calcul de boites): " + str(TOLERANCE))
    logging.debug("- lissage: " + str(SMOOTHLINE))
    logging.debug("- méthode lissage: Bspline")
    logging.debug("- réduction de sommets lors du lissage: " + str(REDUCTION_LISSAGE))
    logging.debug("- offset (distance ligne de base-lignes parallèles): " + str(OFFSET))
    logging.debug("- écart entre points sur la ligne de base: " + str(ECART))
    logging.debug("- simplification: " + FICHIER_LIGNE_BASE)
    """
    logging.debug("Calcul")
    # LOADING BASELINE
    gpd_baseline = importFileAsGeometry(FICHIER_LIGNE_BASE)
    baseline = gpd_baseline.iloc[0]["geometry"]
    logging.debug('- chargement effectue')
 
    x,y = baseline.coords.xy
    DATA = list(zip(x,y))
    lineout= geos.LineString(DATA, srid=2154)
    processResult.linestring_baseline = lineout
    logging.debug("- store baseline in database")

    # SIMPLIFICATION  
    if SIMPLIFY:
        simplifiedBaseLine = simplifyLine(baseline, TOLERANCE) # simplification
    else:
        simplifiedBaseLine = baseline # no treatment

    # SMOOTHING
    smoothedLine = None
    if SMOOTHLINE:
        #smoothedLine = smoothLine(simplifiedBaseLine, REDUCTION_LISSAGE) #lissage
        smoothedLine = smoothGaussian(simplifiedBaseLine,REDUCTION_LISSAGE)
    else:
        smoothedLine = simplifiedBaseLine # no treatment
        
    # PARALLEL LINES
    paralleles = process_parallel_line(smoothedLine, OFFSET) 
    offset_l = paralleles[0]
    offset_r = paralleles[1]

    # POINTS on Base Line 
    points = process_points(smoothedLine, ECART)

    # TRANSECTS 
    # Méthod V1 
    #transects, transects_base = process_transects(points, offset_l, offset_r)

    # Méthod V2 : passing point on base line is not mandatory
    transectlines = []
    transects, transects_base = process_transects_v2(points, offset_l, offset_r, transectlines)
   
    # Transects intermédiaires (complément si trop distants) - option
    #transectsAdded = check_create_transect_split2(transects, offset_l, offset_r)

    # BOXES
    #boxes = {'area_ratio': [], 'shape': [], 'geometry': [], 'box_id':[], 'cat_id':[], 'rl_segment_ratio':[]}
    #centroids = {'geometry': [],'distCentroidToBaseLine': []}
    #boxes = {'area_ratio': [], 'shape': [], 'geom': [], 'box_id':[], 'cat_id':[], 'rl_segment_ratio':[], 'distCentroidToBaseLine':[], 'centroid_geom':[]}
    boxes = []
    pts1, pts2, segment_offset_l,segment_offset_r, lines1, lines2 = process_boxes(processResult, transects, offset_l, offset_r, boxes, OFFSET, ECART,smoothedLine)

    # EXPORT RESULTS
    po_l = gpd.GeoSeries(offset_l)
    po_r = gpd.GeoSeries(offset_r)

    centroids =  []
    for b in boxes:
        centroids.append(b["centroid_geometry"])

    try:
        x,y = smoothedLine.coords.xy
        DATA = list(zip(x,y))
        processResult.linestring_preparedLine = geos.LineString(DATA, srid=2154)

        x,y = offset_l.coords.xy
        DATA = list(zip(x,y))
        processResult.linestring_po_l = geos.LineString(DATA, srid=2154)

        x,y = offset_r.coords.xy
        DATA = list(zip(x,y))
        processResult.linestring_po_r =  geos.LineString(DATA, srid=2154)

        processResult.processed = True
    except Exception as e:
        processResult.processed = False
        logging.error(f"Processing failed during creation of GEOS objects. Reason: {e}")

    end_time = time.time()
    processResult.processingTime =  round(end_time-start_time, 3)

    logging.info(f" -> Nombre de transects créées : {len(transectlines)}") 
    logging.info(f"Processing {'done' if processResult.processed else 'failed'}. Time spent: {processResult.processingTime} seconds.")
   
    return processResult, boxes, transectlines

