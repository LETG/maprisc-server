# Generated by Django 3.2.15 on 2022-10-13 14:40

from django.conf import settings
import django.contrib.gis.db.models.fields
import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='File',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='created')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='updated')),
                ('is_active', models.BooleanField(db_index=True, default=True)),
                ('baseline_file', models.FileField(blank=True, null=True, upload_to='')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Boxes',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='created')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='updated')),
                ('is_active', models.BooleanField(db_index=True, default=True)),
                ('name', models.CharField(blank=True, max_length=100, null=True)),
                ('baseline_name', models.CharField(blank=True, default=0, max_length=150)),
                ('linestring_baseline', django.contrib.gis.db.models.fields.LineStringField(blank=True, null=True, srid=2154)),
                ('linestring_preparedLine', django.contrib.gis.db.models.fields.LineStringField(blank=True, null=True, srid=4326)),
                ('array_po_r', django.contrib.postgres.fields.ArrayField(base_field=django.contrib.gis.db.models.fields.LineStringField(blank=True, null=True, srid=2154), blank=True, null=True, size=None)),
                ('array_po_l', django.contrib.postgres.fields.ArrayField(base_field=django.contrib.gis.db.models.fields.LineStringField(blank=True, null=True, srid=2154), blank=True, null=True, size=None)),
                ('array_centroids', django.contrib.postgres.fields.ArrayField(base_field=django.contrib.gis.db.models.fields.PointField(blank=True, null=True, srid=2154), blank=True, null=True, size=None)),
                ('toCut', models.BooleanField(default=False)),
                ('cutting_polygon', models.CharField(max_length=150)),
                ('toSimplify', models.BooleanField(default=False)),
                ('simplification_tolerance', models.IntegerField()),
                ('transects_distance', models.IntegerField(default=100)),
                ('offset_to_parallels', models.IntegerField(default=100)),
                ('toSmooth', models.BooleanField(default=True)),
                ('smoothing_value', models.IntegerField()),
                ('processed', models.BooleanField(default=False)),
                ('processingTime', models.CharField(blank=True, default=0, max_length=50)),
                ('array_result_boxes', django.contrib.postgres.fields.ArrayField(base_field=django.contrib.gis.db.models.fields.PolygonField(blank=True, null=True, srid=2154), blank=True, null=True, size=None)),
                ('points_count', models.IntegerField(default=0)),
                ('boxes_count', models.IntegerField(default=0)),
                ('right_fails', models.IntegerField(default=0)),
                ('left_fails', models.IntegerField(default=0)),
                ('magnetic_Rpeaks', models.IntegerField(default=0)),
                ('magnetic_Lpeaks', models.IntegerField(default=0)),
                ('rejectedShapes', models.IntegerField(default=0)),
                ('eventsLog', models.TextField(blank=True)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
