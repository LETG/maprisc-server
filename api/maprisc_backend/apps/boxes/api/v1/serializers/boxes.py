from rest_framework import serializers
from maprisc_backend.apps.boxes.models.boxes import Box, TransectLine

from maprisc_backend.apps.boxes.models import File, Boxes


class BoxeListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Boxes
        fields = ('uuid', 'created_by','updated', 'name', 'processed','baseline_name', 'boxes_count')

class BoxSerializer(serializers.ModelSerializer):
    class Meta:
        model = Box
        fields ='__all__'

class TransectLinesSerializer(serializers.ModelSerializer):
    class Meta:
        model = TransectLine
        fields ='__all__' #('transectline_id', 'geometry')

class BoxesSerializer(serializers.ModelSerializer):
    result_boxes = BoxSerializer(many=True, required=False)
    transect_lines = TransectLinesSerializer(many=True, required=False)
    
    class Meta:
        model = Boxes
        fields ='__all__'
    
    def create(self, validated_data):
        result_boxes_data = validated_data.pop('result_boxes')
        transect_lines_data = validated_data.pop('transect_lines')
        
        boxes = Boxes.objects.create(**validated_data)

        # boxes
        for b in result_boxes_data:
            bo = Box.objects.create(**b)
            boxes.result_boxes.add(bo)

        # transects
        for t in transect_lines_data:
            tl = TransectLine.objects.create(**t)
            boxes.transect_lines.add(tl)

        return boxes
        

        # Alternative (principe)
        """boxes = Boxes.objects.create(**validated_data)
        result_boxes_data = validated_data.pop('result_boxes')
        for b in result_boxes_data:
            b['boxes'] = boxes
        boxes_serializer = self.fields['result_boxes']
        boxes_serializer.create(result_boxes_data)
        transect_lines_data = validated_data.pop('transect_lines')
        for t in transect_lines_data:
            t['boxes'] = boxes
        transect_serializer = self.fields['result_lines']
        transect_serializer.create(transect_lines_data)"""


class FileSerializer(serializers.ModelSerializer):
    class Meta():
        model = File
        fields ='__all__'
