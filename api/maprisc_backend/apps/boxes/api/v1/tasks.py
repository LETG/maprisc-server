from django.conf import settings

from rest_framework import status
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError

import logging
import os
import shutil
import zipfile

from celery import shared_task

from maprisc_backend.apps.boxes.models import Boxes, File
from maprisc_backend.apps.boxes.utils.process import processToBoxes
from maprisc_backend.apps.boxes.api.v1.serializers.boxes import BoxeListSerializer, BoxesSerializer, FileSerializer

@shared_task
def async_boxes_processing(input_data: dict, upload_path: str, user_path: str, baseline_filename: str, baseline_fullpath_source: str, *args, **kwargs) -> dict:
    """
    Celery based asynchronous function to process Boxes from an input baseline

    Params:
    ----
    - input_data: JSON serialized dictionary with processing parameters
    - upload_path: path to the main temporary directory where zip input file is stored
    - user_path: path to the user temporary directory with shp file
    - baseline_filename: original name of the zip file
    - baseline_fullpath_source: path to the baseline input file
    - *args, **kwargs: optional arguments passed to Celery processor. Mostly process metadata being used when requesting Celery/Flower API.
    """
    
    logging.info("Start processing boxes…")
    processResult, processedBoxes, transectLines = processToBoxes(input_data, user_path, baseline_fullpath_source)

    # Préparation de la réponse
    dataRes = {
        'name': input_data['name'],
        'created_by': input_data['created_by'],
        'baseline_name': baseline_filename,
        'toSimplify': input_data['toSimplify'],
        'simplification_tolerance': input_data['simplification_tolerance'],
        'transects_distance': input_data['transects_distance'],
        'offset_to_parallels': input_data['offset_to_parallels'],
        'toSmooth': input_data['toSmooth'],
        'smoothing_value': input_data['smoothing_value'],

        'processed': processResult.processed,
        'boxes_count' : processResult.boxes_count, 
        'linestring_baseline' : processResult.linestring_baseline,
        'linestring_preparedLine' : processResult.linestring_preparedLine,
        'linestring_po_r' : processResult.linestring_po_r,
        'linestring_po_l' : processResult.linestring_po_l,
        'result_boxes' : processedBoxes,
        'transect_lines' : transectLines,
        'points_count' : processResult.points_count,
        'right_fails' : processResult.right_fails,
        'left_fails' : processResult.left_fails,
        'magnetic_Rpeaks' : processResult.magnetic_Rpeaks,
        'magnetic_Lpeaks' : processResult.magnetic_Lpeaks,
        'processingTime' : processResult.processingTime,
        'rejectedShapes': processResult.rejectedShapes,
        'eventsLog': processResult.eventsLog,
    }
    serializerResult = BoxesSerializer(data=dataRes)
    logging.debug("cleaning temporary files ...")
    print("cleaning temporary files ...")
    try:
        if os.path.exists(baseline_fullpath_source):

            file_list=os.listdir(upload_path)
            for i in file_list:
                os.remove(i)
                logging.debug("removing file UPLOAD_PATH ", i)
                print("removing file UPLOAD_PATH ", i)

            file_list=os.listdir(user_path)
            for i in file_list:
                os.remove(i)
            shutil.rmtree(user_path)
            os.remove(baseline_fullpath_source)
            logging.debug("files cleaned up")
            print("files cleaned up")

        else:
            shutil.rmtree(upload_path)
            logging.info("File cleaning done.")
            print("File cleaning done.")
    except FileNotFoundError as error:
            logging.error(f"Could not delete file, file not found: {error}")
            print(f"Could not delete file, file not found: {error}")
            pass
    logging.debug("File cleaning done.")
    print("File cleaning done.")
    

    if serializerResult.is_valid():
        serializerResult.save()    
        logging.info(f"Boxes serializer valid and saved. Done")  
        return serializerResult.data
    else:
        logging.error(f"Boxes serializer invalid: {serializerResult.errors}")
        raise Exception(f"Processing failed. Reason: {serializerResult.errors}")