from django.urls import path

from maprisc_backend.apps.boxes.api.v1.views.boxes import BoxesList, BoxesDetail

urlpatterns = [
    path("boxes/", BoxesList.as_view(), name="boxes-list"),
    path("boxes/<uuid:box_uuid>/", BoxesDetail.as_view(), name="boxes-detail"),
] 
