import os
import shutil
import zipfile
import logging
import json

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.parsers import MultiPartParser
from rest_framework.exceptions import NotFound

from maprisc_backend.apps.boxes.exceptions import InvalidBaseLineError, InvalidFilesError

from django.core.serializers.json import DjangoJSONEncoder
from django.http import Http404, HttpResponse
from django.conf import settings

from maprisc_backend.apps.boxes.exceptions import InvalidFilesError
from maprisc_backend.apps.boxes.permissions import IsOwnerOnly
from maprisc_backend.apps.common.models import UserAction
from maprisc_backend.apps.boxes.models import Boxes
from maprisc_backend.apps.boxes.api.v1.serializers.boxes import BoxeListSerializer, BoxesSerializer, FileSerializer
from maprisc_backend.apps.boxes.utils.process import importFileAsGeometry
from maprisc_backend.apps.boxes.api.v1.tasks import async_boxes_processing

class BoxesList(ListCreateAPIView):
    """ 
    POST action for boxes
    """

    queryset = Boxes.objects.all()
    serializer_class = BoxeListSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        # Filter transects based on user identity (token based)
        queryset = Boxes.objects.filter(created_by=self.request.user.uuid)
        return queryset

    # Define parser class as MultiPartParser
    parser_classes = (MultiPartParser, )
    def post(self, request, ):
        user = request.user
        fileparam = request.data["baseline_file"]

        logging.info(f"POST a new boxes project for user {str(request.user.uuid)}")
        
        # Main serialization
        data = request.data
        data["created_by"] = request.user.uuid

        serializedParams = BoxesSerializer(data = data)
        if serializedParams.is_valid():
            # Serializing baseline input file for immediate/temporary storage
            file_serializer = FileSerializer(data = data)
            if (not file_serializer.is_valid()):
                raise ValidationError(f"Baseline file input parameter is incorrect {file_serializer.errors}")
            else:
                file_object = file_serializer.save()
                logging.debug(f"File serializer saved: {file_object}, {type(file_object)}, {file_object.baseline_file}")

            # Initializing working directories
            UPLOAD_PATH=settings.MEDIA_ROOT + "/"
            USER_PATH=UPLOAD_PATH+ str(user.uuid) + "/" 

            # Creating user directory
            if not os.path.exists(USER_PATH):
                os.makedirs(USER_PATH) 

            logging.debug("UPLOAD PATH=", UPLOAD_PATH)   
            print("UPLOAD PATH=", UPLOAD_PATH)
            logging.debug("USER_PATH = ", USER_PATH)
            print("USER_PATH = ", USER_PATH)

            baselinefilename =  str(fileparam)
            filename =UPLOAD_PATH + baselinefilename 

            logging.debug("filename = ", filename)
            print("filename = ", filename)

            # Extract shapefiles from input zip file
            try:
                logging.debug("trying to extract ", filename, " in ", USER_PATH)
                print("trying to extract ", filename, " in ", USER_PATH)
                with zipfile.ZipFile(filename, 'r') as zip_ref:
                    zip_ref.extractall(USER_PATH)
            except zipfile.BadZipFile as error:
                raise ValidationError(f"Error reading input zip file: {error}")

            fileextr = baselinefilename.replace('.zip','') 
            baseline_fullpath_source = USER_PATH + fileextr + ".shp"
            logging.debug(f"baseline_fullpath_source: {baseline_fullpath_source}")
            try:
                if not os.path.exists(baseline_fullpath_source):
                    logging.error(f"No shapefile found in the input file: {baseline_fullpath_source}")
                    return Response("Le fichier zip transmis ne contient pas une ligne de base (shapefile) valide", status=status.HTTP_400_BAD_REQUEST)

            except FileNotFoundError as error:
                logging.error("Le fichier transmis ne peut pas être traité par le serveur")
                raise InvalidFilesError("Le fichier transmis ne peut pas être traité par le serveur")

            # Check input file if it contains multi-part geometries
            result = importFileAsGeometry(filename)
            if len(result) > 1:
                logging.debug(f"Detected multi-part geometry from input file: {result.shape}, {result.head()}")
                # Input geometry is multi-part. Return 400 error with explicit message to the user: geometry should be single part
                return Response(f"Le fichier d'entrée contient une géométrie multiple ne pouvant être traitée. \
                            Éditer la ligne pour fusionner les géométries ou les traiter dans des fichiers séparés", status=status.HTTP_400_BAD_REQUEST)
            else:
                # Input geometry is single-part (LineString), continue
                logging.debug(f"Detected single-part geometry from input file, everything OK.")
                # Aynschronous call to box processing task.
                # Sending JSON representation (.data) of BoxesSerializer because Celery is expecting JSON
                for key, value in serializedParams.data.items():
                    logging.debug(f"Serialized data: {key}, {value}, {type(value)}")
                async_call = async_boxes_processing.delay(serializedParams.data, UPLOAD_PATH, USER_PATH, baselinefilename, baseline_fullpath_source, json_args=json.dumps({"user": request.user.uuid, "name": data["name"]}, cls=DjangoJSONEncoder))
                # Store user request in database table
                UserAction.objects.create(action_url=request.path_info, http_request_type=request.method, additional_info="", user=request.user)
                return Response( { "status": async_call.status, "task_id": async_call.task_id }, status=status.HTTP_202_ACCEPTED)
        else:
            return Response(f"Les données d'entrée sont invalides. Cause: {serializedParams.errors}", status=status.HTTP_400_BAD_REQUEST)

class BoxesDetail(APIView):

    """
    GET / DELETE  boxe full info (details)
    """

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated, IsOwnerOnly]
    serializer_class = BoxesSerializer
    

    def get_object(self, box_uuid):
        try:
            return Boxes.objects.get(uuid=box_uuid)
        except Boxes.DoesNotExist:
            logging.debug(f"Process not found with id {str(box_uuid)}")
            raise Http404

    def get(self, request, box_uuid, format=None):
        box = self.get_object(box_uuid)
        serializer = BoxesSerializer(box)
        return Response(serializer.data)

    def delete(self, request, box_uuid, format=None):
        box = self.get_object(box_uuid)
        logging.info(f"DELETE - box detail {box.name}, {str(box.uuid)}")
        box.delete()
        return Response(status=status.HTTP_200_OK)

