from django.contrib.gis.db import models as gis_models
from django.contrib.gis.geos import Point
from django.contrib.postgres.fields import ArrayField
from django.db.models import FileField, CASCADE, JSONField
from maprisc_backend.apps.common.models import CoreModel
from maprisc_backend.apps.accounts.models import UserAccount

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.user.id, filename)

# Baseline file
class File(CoreModel):
    baseline_file =  FileField(null=True, blank=True, upload_to='')


class Box(CoreModel):
    # box datas
    box_id = gis_models.BigIntegerField(default=0)
    area_ratio = gis_models.DecimalField(max_digits=3, decimal_places=2)
    shape = gis_models.DecimalField(max_digits=10, decimal_places=2)
    rl_segment_ratio = gis_models.DecimalField(max_digits=8, decimal_places=2)
    cat_id = gis_models.IntegerField(default=0)
    geometry = gis_models.PolygonField(blank=True, null=True, srid=2154)
    width = gis_models.DecimalField(max_digits=10, decimal_places=2)

    # centroid datas
    distCentroidToBaseLine = gis_models.DecimalField(max_digits=12, decimal_places=2)
    centroid_geometry = gis_models.PointField(blank=True, null=True, srid=2154)

    def __str__(self):
        return str(self.box_id)

# Transect Line model
class TransectLine(CoreModel):
    transectline_id = gis_models.BigIntegerField(default=0)
    geometry = gis_models.LineStringField(blank=True, null=True, srid=2154)
    
    def __str__(self):
        return str(self.transectline_id)

# Boxes model
class Boxes(CoreModel):

    # Generic params
    name = gis_models.CharField(max_length=100, blank=True, null=True)
    created_by = gis_models.ForeignKey(UserAccount, on_delete=CASCADE)
    
    # Input file for baseline
    baseline_name=  gis_models.CharField(default=0, max_length=150, blank=True)
    linestring_baseline  = gis_models.LineStringField(blank=True, null=True, srid=2154)
    linestring_preparedLine = gis_models.LineStringField(blank=True, null=True, srid=2154)
    linestring_po_l = gis_models.LineStringField(blank=True, null=True, srid=2154)
    linestring_po_r = gis_models.LineStringField(blank=True, null=True, srid=2154)

    # Params for boxes processing 
    toSimplify = gis_models.BooleanField(default=False)
    simplification_tolerance =  gis_models.IntegerField(default=0)
    transects_distance = gis_models.IntegerField(default=100)
    offset_to_parallels = gis_models.IntegerField(default=100)
    toSmooth =  gis_models.BooleanField(default=True)
    smoothing_value = gis_models.IntegerField(default=0)

    # attributes out of processing 
    processed = gis_models.BooleanField(default=False)
    processingTime = gis_models.CharField(default=0, max_length=50, blank=True)


    points_count = gis_models.IntegerField(default=0)
    boxes_count = gis_models.IntegerField(default=0)
    right_fails = gis_models.IntegerField(default=0)
    left_fails = gis_models.IntegerField(default=0)
    magnetic_Rpeaks = gis_models.IntegerField(default=0)
    magnetic_Lpeaks = gis_models.IntegerField(default=0)
    rejectedShapes = gis_models.IntegerField(default=0)

    # Processing logs
    eventsLog = gis_models.TextField(blank = True)

    result_boxes = gis_models.ManyToManyField(Box, related_name='result_boxes', blank=True)

    # transects
    transect_lines = gis_models.ManyToManyField(TransectLine, related_name='transect_lines', blank=True)