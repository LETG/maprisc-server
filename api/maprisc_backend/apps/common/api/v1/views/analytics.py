from django.db.models import Count, Avg, Sum

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework_simplejwt.authentication import JWTAuthentication

from maprisc_backend.apps.accounts.models import UserAccount
from maprisc_backend.apps.dune.models import Transect, DatedProfile

class AnalyticsView(APIView):
    """
    Provides a summary of API stats

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated, IsAdminUser]

    def summarize(self, request, *args, **kwargs):
        """This can be moved to a Mixin class."""
        # Get user list
        user_queryset = UserAccount.objects.all()
        # Get user activity per module
        #### DUNE ####
        # Get count of transects with number of dates for each
        transect_queryset = Transect.objects.annotate(num_dates=Count('dated_profiles'))
        total_number_transect = transect_queryset.count()
        # Get list of user with at least 1 transect
        dune_user_queryset = UserAccount.objects.annotate(num_transects=Count('transects')).filter(num_transects__gt=0)
        total_dune_user = dune_user_queryset.count()
        # Get count of dated profiles
        dated_profile_queryset = DatedProfile.objects.all()
        total_number_dated_profile = dated_profile_queryset.count()
        # do statistics here, e.g.
        stats = {
            'users': {
                'user_count': user_queryset.count(),
                'user_list': [{
                    'uuid': user.uuid,
                    'email': user.email,
                    } for user in user_queryset ]
            },
            'modules': {
                'dune': {
                    'user_count': total_dune_user,
                    'transect_count': total_number_transect,
                    'date_count': total_number_dated_profile,
                    'avg_transect_per_user': dune_user_queryset.aggregate(Avg('num_transects'))['num_transects__avg'],
                }
            }
        }
        # not using a serializer here since it is already a 
        # form of serialization
        return Response(stats)

    def get(self, request, *args, **kwargs):
        return self.summarize(request, *args, **kwargs)