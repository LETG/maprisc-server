from django.http import Http404
import logging

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication

import requests

class FlowerTaskList(APIView):
    """Lists all tasks from Flower and return JSON result.
    """
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        # Use docker container name (here `flower`) instead of "localhost"
        url = 'http://flower:5555/api/tasks'
        response = requests.get(url)
        try:
            if response.status_code != 200:
                response.raise_for_status()
        except Exception as e:
            logging.info("Error connecting to Flower api : %s", e)
            return Response("Erreur de connexion à l'API Flower", status=e.response.status_code)
        else:
            data = response.json()
            return Response(data, status=status.HTTP_200_OK)