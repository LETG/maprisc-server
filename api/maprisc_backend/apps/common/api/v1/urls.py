from django.urls import path

from maprisc_backend.apps.common.api.v1.views.flower import FlowerTaskList
from maprisc_backend.apps.common.api.v1.views.analytics import AnalyticsView 

urlpatterns = [
    # Specific route to call "external" Flower API (DRF acts as a middleware)
    path("flower/tasks", FlowerTaskList.as_view(), name="flower-task-list"),
    # Summary route for analytics
    path("stats", AnalyticsView.as_view(), name="analytics"),
]
