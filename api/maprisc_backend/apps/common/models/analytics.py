from uuid import uuid4

from django.contrib.gis.db import models

class UserAction(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    action_url = models.CharField(max_length=255)
    http_request_type = models.CharField(max_length=20)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    additional_info = models.CharField(max_length=255)
    user = models.ForeignKey(
        "accounts.UserAccount",
        on_delete=models.CASCADE,
        related_name='actions'
    )