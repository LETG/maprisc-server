from django.apps import AppConfig


class CommonConfig(AppConfig):
    name = "maprisc_backend.apps.common"
