from django.urls import reverse
from django.utils.translation import gettext

from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

import pytest

from maprisc_backend.apps.accounts.models.user_account import UserAccount
from maprisc_backend.apps.common.models import UserAction


@pytest.mark.django_db
def test_store_user_action():
    assert UserAction.objects.count() == 0

    user_email = "jane@example.com"
    user_password = "super-secret-password"  # nosec
    user = UserAccount.objects.create_user(user_email, user_password)

    action_url = "/api/v1/common/test"
    action_infos = "{'info1': 'Some info'}"
    action_http_request_type = "GET"
    action = UserAction.objects.create(action_url=action_url, http_request_type=action_http_request_type, additional_info=action_infos, user=user)

    assert UserAction.objects.count() == 1
    assert UserAction.objects.first() == action
    assert action.action_url == action_url
    assert action.user == user
    assert action.additional_info == action_infos
    assert action.http_request_type == action_http_request_type
