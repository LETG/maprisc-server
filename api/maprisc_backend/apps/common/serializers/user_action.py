from rest_framework import serializers

from maprisc_backend.apps.common.models import UserAction

class UserActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAction
        fields = ("action_url", "http_request_type", "timestamp", "additional_info", "user")