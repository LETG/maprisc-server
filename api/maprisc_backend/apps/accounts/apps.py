from django.apps import AppConfig


class AccountConfig(AppConfig):
    name = "maprisc_backend.apps.accounts"

    def ready(self) -> None:
        import maprisc_backend.apps.accounts.signals.reset_password  # noqa
