from django.urls import reverse
from django.utils.translation import gettext

from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

import pytest

from maprisc_backend.apps.accounts.models.user_account import UserAccount
from maprisc_backend.apps.common.models import UserAction


LOGIN_SERIALIZER_PATH = "maprisc_backend.apps.accounts.api.v1.serializers.login.LoginSerializer"

@pytest.mark.django_db
def test_obtain_jwt_api_success(unauthorized_api_client):
    data = {"email": "jane@example.com", "password": "super-secret-password"}  # nosec
    user = UserAccount.objects.create_user(data["email"], data["password"])

    jwt_response = unauthorized_api_client.post(reverse("api-v1-accounts:token_obtain_pair"), {"email": data["email"], "password": data["password"]})
    assert jwt_response.status_code == status.HTTP_200_OK
    assert all(key in jwt_response.data for key in ['access', 'refresh'])

    # check user action has been recorded
    assert UserAction.objects.count() == 1
    action = UserAction.objects.first()
    assert action.action_url == "/api/v1/accounts/token/"
    assert action.additional_info == ""
    assert action.http_request_type == "POST"

@pytest.mark.django_db
def test_refresh_jwt_api_success(unauthorized_api_client):
    data = {"email": "jane@example.com", "password": "super-secret-password"}  # nosec
    user = UserAccount.objects.create_user(data["email"], data["password"])

    jwt_response = unauthorized_api_client.post(reverse("api-v1-accounts:token_obtain_pair"), {"email": data["email"], "password": data["password"]})
    assert jwt_response.status_code == status.HTTP_200_OK
    assert all(key in jwt_response.data for key in ['access', 'refresh'])

    refresh_jwt_response = unauthorized_api_client.post(reverse("api-v1-accounts:token_refresh"), {"refresh": jwt_response.data["refresh"]})
    assert refresh_jwt_response.status_code == status.HTTP_200_OK
    assert all(key in jwt_response.data for key in ['access', 'refresh'])


