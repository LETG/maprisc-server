from django.urls import reverse

from rest_framework import status

import pytest


@pytest.mark.django_db
def test_user_profile_api_get_success(user_account, api_client):
    email = "john@example.com"
    first_name = "John"
    last_name = "Doe"
    structure = "my_structure"
    structure_type = "university"
    position = "engineer"
    user = user_account(email=email, first_name=first_name, last_name=last_name, structure=structure, structure_type=structure_type, position=position)
    client = api_client(auth_user=user)

    response = client.get(reverse("api-v1-accounts:user-profile"))

    assert response.status_code == status.HTTP_200_OK
    assert response.data["email"] == email
    assert response.data["first_name"] == first_name
    assert response.data["last_name"] == last_name
    assert response.data["structure"] == structure
    assert response.data["structure_type"] == structure_type
    assert response.data["position"] == position
    assert response.data["is_active"] == True
    assert response.data["is_staff"] == False


@pytest.mark.django_db
def test_user_profile_api_update_success(user_account, api_client):
    email = "john@example.com"
    old_first_name = "John"
    old_last_name = "Doe"
    new_first_name = "Jane"
    structure = "my_structure"
    structure_type = "university"
    position = "engineer"
    user = user_account(email=email, first_name=old_first_name, last_name=old_last_name, structure=structure, structure_type=structure_type, position=position)
    client = api_client(auth_user=user)

    response = client.put(reverse("api-v1-accounts:user-profile"), {"first_name": new_first_name})

    assert response.status_code == status.HTTP_200_OK
    user.refresh_from_db()
    assert user.email == email
    assert user.first_name == new_first_name
    assert user.last_name == old_last_name
