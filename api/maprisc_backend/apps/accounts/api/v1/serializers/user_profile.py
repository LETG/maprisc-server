from django.utils.translation import gettext_lazy as _

from rest_framework import serializers, exceptions
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from maprisc_backend.apps.accounts.models import UserAccount
from maprisc_backend.apps.common.models import UserAction

class UserProfileSerializer(serializers.ModelSerializer):
    email = serializers.ReadOnlyField()
    is_staff = serializers.ReadOnlyField()
    is_active = serializers.ReadOnlyField()

    class Meta:
        model = UserAccount
        fields = ("uuid", "email", "first_name", "last_name", "structure", "structure_type", "position", "is_staff", "is_active", "last_login")

class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    """
    Check if the user exists and is active before returning pair of token keys.
    """

    error_msg = "Aucun compte actif n'a été trouvé avec ces identifiants."
    default_error_messages = {
        'no_active_account': _(error_msg)
    }

    def validate(self, attrs):

        # The default result (access/refresh tokens)
        data = super(CustomTokenObtainPairSerializer, self).validate(attrs)

        # Custom data you want to include
        data.update({'first_name': self.user.first_name, 'last_name': self.user.last_name})
        data.update({'structure': self.user.structure, 'structure_type': self.user.structure_type, 'position': self.user.position})
        data.update({'is_staff': self.user.is_staff})
        data.update({'is_active': self.user.is_active})

        # Store user login in database table
        UserAction.objects.create(action_url=self.context['request'].path_info, http_request_type=self.context['request'].method, additional_info="", user=self.user)
        
        return data