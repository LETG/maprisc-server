from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, serializers
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.views import TokenObtainPairView

from maprisc_backend.apps.accounts.api.v1.serializers.user_profile import UserProfileSerializer, CustomTokenObtainPairSerializer
from maprisc_backend.apps.accounts.models import UserAccount

class UserProfileAPIView(RetrieveUpdateAPIView):

    serializer_class = UserProfileSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user

class CustomTokenObtainPairView(TokenObtainPairView):
    """
    View to retrieve custom user info based on token auth instead of default access and refresh only
    """
    # Replace the serializer with your custom
    serializer_class = CustomTokenObtainPairSerializer

class UserListAPIView(APIView):
    """
    View to list all users in the system and to update them.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAdminUser]

    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        users = [user for user in UserAccount.objects.all()]
        serializer = UserProfileSerializer(users, many=True)
        return Response(serializer.data)

    def get_object_by_email(self, obj_email):
        try:
            return UserAccount.objects.get(email=obj_email)
        except (UserAccount.DoesNotExist, serializers.ValidationError):
            raise serializers.ValidationError("Le compte n'existe pas", status.HTTP_400_BAD_REQUEST)
    
    def validate_emails(self, email_list):
        for email in email_list:
            try:
                UserAccount.objects.get(email=email)
            except (UserAccount.DoesNotExist, serializers.ValidationError):
                raise serializers.ValidationError("Le compte n'existe pas", status.HTTP_400_BAD_REQUEST)
        return True

    def put(self, request, *args, **kwargs):
        data = request.data
        email_list = [ item['email'] for item in data ]
        self.validate_emails(email_list=email_list)
        instances = []
        for update_user in data:
            email = update_user['email']
            is_staff = update_user['is_staff']
            is_active = update_user['is_active']
            obj = self.get_object_by_email(obj_email=email)
            obj.is_staff = is_staff
            obj.is_active = is_active
            obj.save()
            instances.append(obj)
        serializer = UserProfileSerializer(instances, many=True)
        return Response(serializer.data)

    def delete(self, request, *args, **kwargs):
        data = request.data
        email_list = [ item['email'] for item in data ]
        self.validate_emails(email_list=email_list)
        for delete_user in data:
            email = delete_user['email']
            obj = self.get_object_by_email(obj_email=email)
            obj.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
