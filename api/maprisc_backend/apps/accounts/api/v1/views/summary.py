from django.db.models import Count, Avg, Sum

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework_simplejwt.authentication import JWTAuthentication

from maprisc_backend.apps.common.models import UserAction
from maprisc_backend.apps.common.serializers.user_action import UserActionSerializer

class UserActionListView(APIView):
    """
    Provides a summary of Maprisc user actions for analytics

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated, IsAdminUser]

    def get(self, request, format=None):
        user_actions = UserAction.objects.all()
        serializer = UserActionSerializer(user_actions, many=True)
        return Response(serializer.data)