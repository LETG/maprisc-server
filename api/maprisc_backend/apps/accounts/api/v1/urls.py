from django.urls import path

from rest_framework_simplejwt.views import (
    TokenRefreshView,
    TokenVerifyView,
)

from maprisc_backend.apps.accounts.api.v1.views.login import LoginView, LogoutView
from maprisc_backend.apps.accounts.api.v1.views.password import (
    ChangePasswordAPIView,
    ConfirmResetPasswordAPIView,
    ResetPasswordAPIView,
)
from maprisc_backend.apps.accounts.api.v1.views.registration import RegistrationAPIView
from maprisc_backend.apps.accounts.api.v1.views.user_profile import UserProfileAPIView, CustomTokenObtainPairView, UserListAPIView
from maprisc_backend.apps.accounts.api.v1.views.summary import UserActionListView 


urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("me/", UserProfileAPIView.as_view(), name="user-profile"),
    path("password/", ChangePasswordAPIView.as_view(), name="change-password"),
    path("password/confirm/", ConfirmResetPasswordAPIView.as_view(), name="confirm-reset-password"),
    path("password/reset/", ResetPasswordAPIView.as_view(), name="reset-password"),
    path("registration/", RegistrationAPIView.as_view(), name="registration"),
    path('token/', CustomTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('list/', UserListAPIView.as_view(), name='user-list'),
    path('stats/', UserActionListView.as_view(), name="user-actions"),
]
