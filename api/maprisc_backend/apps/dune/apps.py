from django.apps import AppConfig


class DuneConfig(AppConfig):
    name = 'maprisc_backend.apps.dune'
