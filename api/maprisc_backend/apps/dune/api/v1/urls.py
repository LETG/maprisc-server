from django.urls import path

from maprisc_backend.apps.dune.api.v1.views.summary import SummaryView 
from maprisc_backend.apps.dune.api.v1.views.transect import TransectList, TransectDetail, TransectImport
from maprisc_backend.apps.common.api.v1.views.flower import FlowerTaskList
from maprisc_backend.apps.dune.api.v1.views.datedprofile import DatedProfileList, DatedProfileDetail


urlpatterns = [
    path("transects/", TransectList.as_view(), name="transect-list"),
    path("transects/<uuid:transect_uuid>/profiles", DatedProfileList.as_view(), name="datedprofile-list"),
    path("transects/<uuid:transect_uuid>", TransectDetail.as_view(), name="transect-detail"),
    path("transects/<uuid:transect_uuid>/profiles/<uuid:profile_uuid>", DatedProfileDetail.as_view(), name="datedprofile-detail"),
    # Summary route for analytics
    path("stats", SummaryView.as_view(), name="dune-summary"),
    # Specific route to call "external" Flower API (DRF acts as a middleware)
    path("flower/tasks", FlowerTaskList.as_view(), name="flower-task-list"),
    # Specific route to import JSON data (reserved access to admins)
    path("import", TransectImport.as_view(), name="import"),
]
