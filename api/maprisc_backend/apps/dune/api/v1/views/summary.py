from django.db.models import Count, Avg, Sum

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework_simplejwt.authentication import JWTAuthentication

from maprisc_backend.apps.dune.models import Transect, DatedProfile
from maprisc_backend.apps.accounts.models import UserAccount
from maprisc_backend.apps.dune.api.v1.serializers.dune_profile import TransectSerializer
from maprisc_backend.apps.dune.permissions import IsOwnerOrReadOnly, IsOwnerOnly

class SummaryView(APIView):
    """
    Provides a summary of dune API stats

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated, IsAdminUser]

    def summarize(self, request, *args, **kwargs):
        """This can be moved to a Mixin class."""
        # Get count of transects with number of dates for each
        transect_queryset = Transect.objects.annotate(num_dates=Count('dated_profiles'))
        total_number_transect = transect_queryset.count()
        # Get list of user with at least 1 transect
        user_queryset = UserAccount.objects.annotate(num_transects=Count('transects')).filter(num_transects__gt=0)
        total_number_user = user_queryset.count()
        # Get count of dated profiles
        dated_profile_queryset = DatedProfile.objects.all()
        total_number_dated_profile = dated_profile_queryset.count()
        # do statistics here, e.g.
        stats = {
            'user_count': total_number_user,
            'transect_count': total_number_transect,
            'date_count': total_number_dated_profile,
            'avg_transect_per_user': user_queryset.aggregate(Avg('num_transects'))['num_transects__avg'],
            'detail': [
                {
                    'user': user.uuid,
                    'transect_count': user.num_transects,
                    'date_count': transect_queryset.filter(user=user.uuid).aggregate(Sum('num_dates'))['num_dates__sum']
                } for user in user_queryset ]
        }
        # not using a serializer here since it is already a 
        # form of serialization
        return Response(stats)

    def get(self, request, *args, **kwargs):
        return self.summarize(request, *args, **kwargs)