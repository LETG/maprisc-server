import json
import logging

from django.http.response import Http404
from django.core.serializers.json import DjangoJSONEncoder

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import RetrieveUpdateAPIView, ListCreateAPIView
from rest_framework.mixins import DestroyModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework_simplejwt.authentication import JWTAuthentication

from maprisc_backend.apps.common.models import UserAction
from maprisc_backend.apps.dune.models import DatedProfile, Transect
from maprisc_backend.apps.dune.api.v1.serializers.dune_profile import TransectSerializer, DatedProfileSerializer
from maprisc_backend.apps.dune.api.v1.tasks import process_datedprofiles, delete_datedprofiles
from maprisc_backend.apps.dune.permissions import IsOwnerOnly

class DatedProfileList(ListCreateAPIView, DestroyModelMixin):
    """
    Lists all dated profiles linked to a transect.
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication, JWTAuthentication]
    permission_classes = [IsAuthenticated, IsOwnerOnly]
    serializer_class = DatedProfileSerializer
    
    def get_queryset(self, transect_uuid):
        """
        This view should return a list of all the dated profiles
        for the selected transect.
        """
        queryset = DatedProfile.objects.filter(transect=transect_uuid)
        if queryset:
            return queryset
        else:
            raise Http404
    
    def list(self, request, *args, **kwargs):
        """
        Overrides list method to display foreign key (transect_uuid) in attributes
        """
        transect_uuid = self.kwargs['transect_uuid']
        queryset = self.get_queryset(transect_uuid)
        serializer = DatedProfileSerializer(queryset, many=True)
        return Response(serializer.data)
    
    def post(self, request, transect_uuid, format=None):
        """
        Overrides ListCreateAPIView post method to process geojson data
        Request data contains an array of dated profiles and processing infos
        """
        transect = Transect.objects.get(uuid=transect_uuid)
        transect_data = TransectSerializer(transect).data

        # Asynchronous call using Celery shared task
        async_call = process_datedprofiles.delay(request.data, transect_data, json_args=json.dumps({"user": request.user.uuid, "name": transect_data["name"], "index": transect_data["index"]}, cls=DjangoJSONEncoder))
        # Store user request in database table
        additional_info = "{{'transect': {}, 'submitted_dates': {}}}".format(transect_uuid, len(request.data["dated_profiles"]))
        UserAction.objects.create(action_url=request.path_info, http_request_type=request.method, additional_info=additional_info, user=request.user)
        return Response( { "status": async_call.status, "task_id": async_call.task_id }, status=status.HTTP_202_ACCEPTED )
    
    def delete(self, request, transect_uuid, *args, **kwargs):
        data = request.data
        transect = Transect.objects.get(uuid=transect_uuid)
        transect_data = TransectSerializer(transect).data

        # Asynchronous call using Celery shared task
        async_call = delete_datedprofiles.delay(data, transect_data)
        return Response( { "status": async_call.status, "task_id": async_call.task_id }, status=status.HTTP_202_ACCEPTED )

class DatedProfileDetail(APIView):
    """
    Retrieve, update or delete a profile instance.
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication, JWTAuthentication]
    permission_classes = [IsAuthenticated, IsOwnerOnly]
    
    def get_object(self, transect_uuid, profile_uuid):
        try:
            return DatedProfile.objects.get(transect=transect_uuid, uuid=profile_uuid)
        except DatedProfile.DoesNotExist:
            raise Http404

    def get(self, request, transect_uuid, profile_uuid, format=None):
        dated_profile = self.get_object(transect_uuid, profile_uuid)
        serializer = DatedProfileSerializer(dated_profile)
        return Response(serializer.data)

    def put(self, request, transect_uuid, profile_uuid, format=None):
        dated_profile = self.get_object(transect_uuid, profile_uuid)
        serializer = DatedProfileSerializer(dated_profile, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, transect_uuid, profile_uuid, format=None):
        dated_profile = self.get_object(transect_uuid, profile_uuid,)
        dated_profile.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)