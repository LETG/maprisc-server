from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework_simplejwt.authentication import JWTAuthentication

import logging
import json
import numpy as np
import base64
import zlib
import collections

from django.contrib.gis.geos import Point, LineString
from django.core.serializers.json import DjangoJSONEncoder

from maprisc_backend.apps.common.models import UserAction
from maprisc_backend.apps.dune.models import Transect
from maprisc_backend.apps.dune.api.v1.serializers.dune_profile import TransectSerializer
from maprisc_backend.apps.dune.api.v1.tasks import process_transect_datedprofiles
from maprisc_backend.apps.dune.permissions import IsOwnerOnly

class TransectList(ListCreateAPIView):
    """
    View dedicated to GET a list of Transect instances or POST a new one
    """    
    serializer_class = TransectSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        # Filter transects based on user identity (token based)
        queryset = Transect.objects.filter(user=self.request.user)
        return queryset
    
    def post(self, request, format=None):

        transect_data = request.data
        # Check if data is gzipped
        content_encoding = request.headers.get('Content-Encoding')
        if content_encoding:
            try:
                # Decompress data if body has been gzipped (and base64 encoded)
                transect_data_b64decoded = base64.b64decode(transect_data)
                transect_data = zlib.decompress(transect_data_b64decoded, zlib.MAX_WBITS|32) # windowBits parameter must have a value of 31 if gzip compression was used
                # Parse the string with JSON data to dict
                transect_data = json.loads(transect_data)
            except Exception as e:
                return Response(e, status=status.HTTP_400_BAD_REQUEST)
        else:
            pass
        # User data is accessible within Django User instance returned by request.user
        transect_data["user"] = request.user.uuid

        if "dated_profiles" in transect_data.keys():
            dated_profiles = transect_data["dated_profiles"]
            profile_head = transect_data["profile_head"]
            profile_axis = transect_data["profile_axis"]
            if not (profile_axis and profile_head):
                return Response(f"No data provided for profile head ({profile_head}), and/or axis ({profile_axis})", status=status.HTTP_400_BAD_REQUEST)
            else:
                if dated_profiles:
                    if type(dated_profiles) is list:
                        # Asynchronous call using celery
                        # Pass optional kwargs as valid JSON string so it can be parsed on JS side from Flower API
                        async_call = process_transect_datedprofiles.delay(transect_data, json_args=json.dumps({"user": request.user.uuid, "name": transect_data["name"], "index": transect_data["index"]}, cls=DjangoJSONEncoder))
                        # Store user request in database table
                        UserAction.objects.create(action_url=request.path_info, http_request_type=request.method, additional_info="{'submitted_dates':"+str(len(dated_profiles))+"}", user=request.user)
                        # Whatever happens during the processing, request have been accepted so returns 202
                        return Response( { "status": async_call.status, "task_id": async_call.task_id }, status=status.HTTP_202_ACCEPTED)
                    else:
                        return Response("Dated profiles property should be a list not %s" % type(dated_profiles).__name__,
                                status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response(f"No profile data provided", status=status.HTTP_400_BAD_REQUEST)
        else:
            # If no time-consuming action is required, create the transect
            # Don't forget to convert profile head and axis to Geometry compliant types
            transect_data["profile_head"] = Point(transect_data["profile_head"])
            transect_data["profile_axis"] = LineString(transect_data["profile_axis"])

            serializer = TransectSerializer(data=transect_data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        

class TransectDetail(RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update or delete a transect instance.
    Access restricted to owner
    """

    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated, IsOwnerOnly]

    serializer_class = TransectSerializer
    lookup_url_kwarg = 'transect_uuid'

    def get_queryset(self):
        uuid = self.kwargs.get(self.lookup_url_kwarg)
        queryset = Transect.objects.filter(uuid=uuid)
        return queryset

    def patch(self, request, transect_uuid, format=None):
        transect = self.get_object()
        serializer = TransectSerializer(transect, data=request.data, partial=True) # set partial=True to update a data partially
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class TransectImport(CreateAPIView):
    """
    View dedicated to POST a nested Transect object
    Use it as a way to import dumped data from another database
    Admin reserved view
    """

    serializer_class = TransectSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated, IsAdminUser]
    
    def post(self, request, format=json):
        transect_data = request.data

        # Check if input data is a dictionary (unique Transect object) or a list (of Transect)
        if isinstance(transect_data, collections.Mapping):
            serializer = TransectSerializer(data=transect_data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        elif isinstance(transect_data, list):
            for transect in transect_data:
                serializer = TransectSerializer(data=transect)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response("Dump data should be valid JSON, either an object or a list of objects",
                            status=status.HTTP_400_BAD_REQUEST)

        