from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer

from maprisc_backend.apps.dune.models import Transect, DatedProfile, DatedProfileVolume


class DatedProfileVolumeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DatedProfileVolume
        fields = '__all__'


class DatedProfileSerializer(GeoFeatureModelSerializer):
    """ A class to serialize dated profiles as GeoJSON compatible data """
    volumes = DatedProfileVolumeSerializer(many=True, required=False)

    class Meta:
        model = DatedProfile
        geo_field = "geom"
        fields = '__all__'

    def create(self, validated_data):
        if 'volumes' in validated_data.keys():
            volumes_validated_data = validated_data.pop('volumes')
            dated_profile = DatedProfile.objects.create(**validated_data)
            volume_serializer = self.fields['volumes']
            for volume in volumes_validated_data:
                volume['dated_profile'] = dated_profile
            volumes = volume_serializer.create(volumes_validated_data)
            return dated_profile
        else:
            return DatedProfile.objects.create(**validated_data)

class TransectSerializer(serializers.ModelSerializer):
    dated_profiles = DatedProfileSerializer(many=True, required=False)

    class Meta:
        model = Transect
        fields = '__all__'

    def create(self, validated_data):
        if 'dated_profiles' in validated_data.keys():
            datedprofiles_validated_data = validated_data.pop('dated_profiles')
            transect = Transect.objects.create(**validated_data)
            datedprofile_serializer = self.fields['dated_profiles']
            for dp in datedprofiles_validated_data:
                dp['transect'] = transect
            dated_profiles = datedprofile_serializer.create(datedprofiles_validated_data)
            return transect
        else:
            return Transect.objects.create(**validated_data)