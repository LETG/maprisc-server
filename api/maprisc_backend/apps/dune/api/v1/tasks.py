from django.contrib.gis.geos import Point, MultiPoint, LineString
from django.utils import timezone

from rest_framework import status
from rest_framework.response import Response
from celery import shared_task
from datetime import datetime
import logging

from maprisc_backend.apps.dune.models import Transect, DatedProfile, DatedProfileVolume
from maprisc_backend.apps.dune.utils.dune import handle_transect_points, compute_profile_volumes
from maprisc_backend.apps.dune.utils.preprocessing import update_geojson_profile_with_distance
from maprisc_backend.apps.dune.api.v1.serializers.dune_profile import DatedProfileSerializer, TransectSerializer

def process_profile_data(data, r1, r2, phma_site, autosearch):
    '''
    Call dune utils main function: process input profile to extract crest and dune

    Parameters
    -------------
    data: Dictionary
        Profile data shaped as a Python dictionary
    r1, r2: String
        Respective codes for middle and upper morphological markers. Not used if autosearch mode is activated.
    autosearch: Boolean
        Define which mode is used to compute profile characteristics : either automatic using signal processing or manual based on user input
    phma_site: Float
        Highest astronomical tide used for crest detection. Mandatory for automatic search. Not used if search_mode is manual.
    
    Returns Python dict object with the following shape:
        profile_data = {
            "date": date,
            "crest_index": crest_index,
            "foot_index": foot_index,
            "uncertainty": uncertainty,
            "geom": point_geom_list,
            "points_position": points_position,
            "volumes": []
        }
    '''
    profile_data = handle_transect_points(data["geojson"], r1, r2, data["uncertainty"], phma_site, autosearch)
    profile_data["date"] = datetime.strptime(data["date"], '%Y-%m-%d').replace(tzinfo=timezone.utc)

    return profile_data

def get_point_list_from_geojson(dated_profile_list):
    '''
    Extract list of point indices from geojson input data
    Indices are based on the a relative distance (input property) from the profile head
    '''

    dated_profiles_points = []
    # Inspect geojson data to get list of points
    try:
        dated_profiles_geojson = [ dp["geojson"] for dp in dated_profile_list ]
        dated_profiles_features = [ geojson["features"] for geojson in dated_profiles_geojson ]
        dated_profiles_points = [ [ feature["properties"]["distance"] for feature in features ] for features in dated_profiles_features ]
    except:
        logging.debug("Problem in input profile data. Please check original data %s", dated_profile_list)
    return dated_profiles_points

def get_reference_distance_from_transect_head(profile_list):
    '''
    Computes a common distance to profile head to be used as a reference for all profiles alignment on the common transect entity.
    The distance is based on the maximum distance property among all points from all input profiles    
    '''

    reference_distance = max([max([ feature["properties"]["distance"] for feature in dated_profile["geojson"]["features"] ]) for dated_profile in profile_list ])

    return reference_distance

def check_input_profile(dated_profile, reference_distance):
    '''
    Check if the profile's direction is correct: point with lowest index should be seaward
    Otherwise, switch direction by translating distance coordinate.
    
    Parameters
    ---------------
    dated_profile: DatedProfile
        DatedProfile object including the date and geojson data
    reference_distance: float
        The distance being used to make the translation of points into relative coordinate system.

    Returns
    ---------------
    Returns dictionary with updated list of profiles and the reference distance (max) to the profile head
    '''

    # First, sort features by distance to the profile head
    dated_profile["geojson"]["features"].sort(key=lambda x: x["properties"]["distance"])
    # Check if the first point has lower z (elevation) value than the last point in the profile
    first_point_coords = dated_profile["geojson"]["features"][0]["geometry"]["coordinates"]
    last_point_coords = dated_profile["geojson"]["features"][-1]["geometry"]["coordinates"]
    if first_point_coords[2] > last_point_coords[2]:
        # switch direction
        logging.info("Input profile orientation needs to be switched: %s / %s", first_point_coords, last_point_coords)
        dated_profile["geojson"]["features"] = dated_profile["geojson"]["features"][::-1]
        # update distance attribute to be reordered
        for feature in dated_profile["geojson"]["features"]:
            feature["properties"]["distance"] = round(reference_distance - feature["properties"]["distance"], 3)

    return {'profile': dated_profile, 'reference_distance': reference_distance}


def compute_common_window(profile_list, dp_point_list, furthest_limit_threshold=50):
    '''
    Compute a common window for further comparison between profiles (sand budget mostly)
    Output window is based on 3 boundaries:
    - first (low) boundary: max index from points with lowest profile index
    - shore/dune interface: min index among dune toes. Dune toes must be within the low and high boundaries, ignore otherwise (can happen if extraction fails or behaves badly).
    - third (high) boundary: min index from points with highest profile index, but no further than 50 meters after the last detected crest (to strip the landward part of the profile)
    '''

    MAX_LIMIT = furthest_limit_threshold

    # Check if we are processing new data (from GeoJSON) or existing ones (from Postgres)
    # In case of existing data, dp_point_list is not provided thus empty
    if len(dp_point_list) > 0:
        window_low_ref = max([ min([indices for indices in dp]) for dp in dp_point_list ])
        min_dune_toe_ref = min([ p['foot_index'] for p in profile_list if p['foot_index'] > window_low_ref ])
        # Compute furthest boundary
        max_point = min([ max([indices for indices in dp]) for dp in dp_point_list ])
        max_crest = max([ p['crest_index'] for p in profile_list ]) #furthest dune crest
    else:
        window_low_ref = max([ min([point[0] for point in dp['properties']['points_position']]) for dp in profile_list ])
        min_dune_toe_ref = min([ dp['properties']['foot_index'] for dp in profile_list if dp['properties']['foot_index'] > window_low_ref ])
        # Compute furthest boundary
        max_point = min([ max([point[0] for point in dp['properties']['points_position']]) for dp in profile_list ])
        max_crest = max([ dp['properties']['crest_index'] for dp in profile_list ]) #furthest dune crest

    if max_point > max_crest + MAX_LIMIT:
        window_high_ref = max_crest + MAX_LIMIT
    else:
        window_high_ref = max_point

    return (window_low_ref, min_dune_toe_ref, window_high_ref)

def update_dated_profiles_volumes(transect_dated_profiles, min_dune_toe_ref, window_low_ref, window_high_ref):
    '''
    Update original data based on new reference dune toe for volumes computation
    '''

    for dp in transect_dated_profiles:
        initial_dated_profile = DatedProfile.objects.get(uuid=dp['id'])
        # Delete existing records in volumes table associated to this dated_profile
        DatedProfileVolume.objects.filter(dated_profile=dp['id']).delete()
        # Recreate data shape for compute_profile_volumes method based on existing data
        point_geom_list = [ [dp['properties']['points_position'][idx][0], feat[0], feat[1], feat[2]] for idx, feat in enumerate(dp['geometry']['coordinates']) ]
        dp_data = {
            'crest_index': dp['properties']['crest_index'],
            'foot_index': dp['properties']['foot_index'],
            'uncertainty': dp['properties']['uncertainty'],
            'geom': point_geom_list,
            'points_position': dp['properties']['points_position'],
            'volumes': []
        }
        dp_data_with_volumes = compute_profile_volumes(dp_data, min_dune_toe_ref, window_low_ref, window_high_ref)

        # Update initial record in database with new associated volumes
        volume_object_list = [ DatedProfileVolume(**obj) for obj in dp_data_with_volumes['volumes'] ]
        initial_dated_profile.volumes.set( volume_object_list, bulk=False ) # Use bulk=false because previously created objects are not saved

@shared_task
def process_datedprofiles(dated_profiles_data, transect_data, *args, **kwargs):
    '''
    Apply dune profile analysis for a set of dated profiles.
    Update associated transect data with new volumes

    Parameters
    ---------------

    dated_profiles_data : Dictionary with processing infos and list of dated profiles data
        Example:
        {
            'r1': "R1",
            'r2': "R2",
            'dated_profiles': [
                {
                    "date": XXXX-MM-DD,
                    "uncertainty": < value >,
                    "geojson": < geojson data >
                }
            ]
        }
    transect_data : dict
        Dictionary representation of Transect object
    '''

    logging.info("Celery task ongoing to process dated profiles for transect %s", transect_data["uuid"])

    dated_profile_list_w_distance = []
    checked_dp_list_w_distance = []
    profiles = []

    # Retrieve transect head and axis from database
    # profile head and axis are dictionary with the following shape : {"type": "", "coordinates": []}
    profile_head = transect_data["profile_head"]["coordinates"]
    profile_axis = transect_data["profile_axis"]["coordinates"]

    # Retrieve autosearch mode info from database to fill new dated profiles
    # Use attribute from parent Transect object as this attribute is inferred to all of the Transect dated profiles
    # TODO Update when different mode can be set for each profile
    autosearch = transect_data["autosearch"]
    if not autosearch:
        # Manual mode requires R1 and R2 morphological markers
        try:
            R1 = dated_profiles_data["r1"]
            R2 = dated_profiles_data["r2"]
        except Exception as error:
            # Add custom message to exception for display purpose
            # Waiting Python >3.11 to use add_note() instead, e.g. error.add_note(error_message)
            error_message = "Error processing input dated profiles. Manual mode requires morphological markers r1/r2."
            logging.error(error_message, error)
            error.args += (error_message,)
            raise error
    else:
        R1 = None
        R2 = None


    dated_profile_list = dated_profiles_data["dated_profiles"]

    # Compute caracteristics for each profile
    for dp in dated_profile_list:
        # Pre-process profiles to compute distance to profile head
        checked_dp = update_geojson_profile_with_distance(dp, profile_head, profile_axis)
        dated_profile_list_w_distance.append(checked_dp)
    logging.debug("Profile example after computing distance: %s", dated_profile_list_w_distance[0])

    # If initial transect was created empty, distance_to_profile_head attribute is null.
    # In such case, we need to compute it from new data
    # NOTE : This can only happen when executing tests and calling API directly. Frontend app force creating new transect with at least 2 input files.
    if not transect_data['distance_to_profile_head']:
        # Once distance is computed for each point, compute reference distance
        reference_distance = get_reference_distance_from_transect_head(dated_profile_list_w_distance)
        logging.debug("Reference distance computed: %s", reference_distance)
        transect_data["distance_to_profile_head"] = reference_distance
        # Save new computed value to database
        transect = Transect.objects.get(uuid=transect_data["uuid"])
        transect.distance_to_profile_head = reference_distance
        transect.save()
    else:
        reference_distance = transect_data['distance_to_profile_head']

    # Process new profiles
    for dated_profile in dated_profile_list_w_distance:
        # Check if profiles are in the right direction (seaward on left)
        result = check_input_profile(dated_profile=dated_profile, reference_distance=reference_distance)
        checked_dp = result['profile']
        checked_dp_list_w_distance.append(checked_dp)

        profile_data = process_profile_data(checked_dp, R1, R2, transect_data["phma"], autosearch)
        profile_data["transect"] = transect_data["uuid"]
        profiles.append(profile_data)

    # Get list of points.
    # WARNING: function parameter should be a GeoJSON-like object
    dated_profile_point_list = get_point_list_from_geojson(checked_dp_list_w_distance)

    # Compute volume based on common window and lower dune toe as shore/dune interface
    window_low_ref, min_dune_toe_ref, window_high_ref = compute_common_window(profiles, dated_profile_point_list)
 
    # Manage existing data related to the same transect: compute new reference for dune toe and update volumes computation
    if len(transect_data["dated_profiles"]["features"]) > 0:
        # Compute common window boundaries with original data if it exists
        transect_dp_data = transect_data["dated_profiles"]["features"]
        updated_window_low_ref, updated_min_dune_toe_ref, updated_window_high_ref = compute_common_window(transect_dp_data, [])
        min_dune_toe_ref = min(min_dune_toe_ref, updated_min_dune_toe_ref)
        window_low_ref = max(window_low_ref, updated_window_low_ref)
        window_high_ref = min(window_high_ref, updated_window_high_ref)

        update_dated_profiles_volumes(transect_dp_data, min_dune_toe_ref, window_low_ref,window_high_ref)

    # Compute volumes for each new dated profile associated to the transect
    dated_profiles_with_volumes = []
    for profile in profiles:
        profile_data = compute_profile_volumes(profile, min_dune_toe_ref, window_low_ref, window_high_ref)
        # Transforms list of points data into GeoDjango MultiPoint feature
        profile_data["geom"] = MultiPoint( [Point(point[1], point[2], point[3]) for point in profile_data["geom"]] )
        # Add autosearch mode to all dated profiles: TODO update when mode can be set for each dataset
        profile_data["autosearch"] = autosearch
        dated_profiles_with_volumes.append(profile_data)

    serializer = DatedProfileSerializer(data=dated_profiles_with_volumes, many=True)

    if serializer.is_valid():
        serializer.save()
        return serializer.data
    else:
        logging.error("DatedProfile serializer invalid: %s", serializer.errors)
        raise serializers.ValidationError(serializer.errors)

@shared_task
def process_transect_datedprofiles(transect_data, *args, **kwargs):
    '''
    Store new nested transect object in database.
    Apply dune profile analysis and extraction methods for each associated date:
    - manual or automatic extraction of dated profile characteristics (dune toe and crest)
    - compute volumes based on common due toe reference
    - save instances to database

    Parameters
    ---------------

    transect_data : dict
        Dictionary representation of Transect object
        {
            'name': Transect name,
            'index': Transect index,
            'autosearch': type of search for dune crest and toe (either automatic or manually defined in the dataset),
            'phma': highest astronomical tide (manually defined threshold)
            'profile_head': point coordinates of profile head as list
            'profile_axis': line coordinates of profile_axis as list
            'r1': middle morphological marker to be used for window computation
            'r2': upper morphological marker to be used for window computation
            'dated_profiles': array of profiles including date and geojson data
        }
    '''
    
    dated_profiles = transect_data['dated_profiles']
    dated_profile_list_w_distance = []
    checked_profile_list_w_distance = []

    profiles_data = []
    new_dated_profiles = []

    # Check search mode: auto or manual
    autosearch = transect_data["autosearch"]
    if not autosearch:
        # Manual mode requires R1 and R2 morphological markers
        try:
            R1 = transect_data["r1"]
            R2 = transect_data["r2"]
        except Exception as error:
            # Add custom message to exception for display purpose
            # Waiting Python >3.11 to use add_note() instead, e.g. error.add_note(error_message)
            error_message = "Error processing input transect dated profiles. Manual mode requires morphological markers r1/r2."
            logging.error(error_message, error)
            error.args += (error_message,)
            raise error
    else:
        R1 = None
        R2 = None

    # Compute caracteristics for each profile
    for dp in dated_profiles:
        # Pre-process profiles to compute distance to profile head
        checked_dp = update_geojson_profile_with_distance(dp, transect_data["profile_head"], transect_data["profile_axis"])
        dated_profile_list_w_distance.append(checked_dp)
    logging.debug("Profile example after computing distance: %s", dated_profile_list_w_distance[0])

    #Once distance is computed for each point, compute reference distance
    reference_distance = get_reference_distance_from_transect_head(dated_profile_list_w_distance)
    logging.debug("Reference distance computed: %s", reference_distance)
    transect_data["distance_to_profile_head"] = reference_distance

    # Process profiles one by one
    for dp in dated_profile_list_w_distance:
        # Check if profiles are in the right direction (seaward on left)
        result = check_input_profile(dated_profile=dp, reference_distance=reference_distance)
        checked_dp = result['profile']
        checked_profile_list_w_distance.append(checked_dp)
        # Process profile
        profile_data = process_profile_data(checked_dp, R1, R2, transect_data["phma"], autosearch)
        profiles_data.append(profile_data)
    logging.debug("Profile example after processing: %s", profiles_data[0])
    
    # Inspect geojson data to get list of points
    # WARNING: function parameter should be a GeoJSON-like object
    dated_profiles_points = get_point_list_from_geojson(checked_profile_list_w_distance)
    logging.debug("Point list from geojson: %s", dated_profiles_points)

    # Compute volume based on common window and lower dune toe as shore/dune interface
    window_low_ref, min_dune_toe_ref, window_high_ref = compute_common_window(profiles_data, dated_profiles_points)
    logging.info("Computed window: low_ref=%s / max_ref=%s / min_toe=%s", window_low_ref, window_high_ref, min_dune_toe_ref)
    
    # Compute volumes for each new dated profile associated to the transect
    for data in profiles_data:
        profile_data = compute_profile_volumes(data, min_dune_toe_ref, window_low_ref, window_high_ref)
        # Transforms list of points data into GeoDjango MultiPoint feature
        profile_data["geom"] = MultiPoint( [Point(point[1], point[2], point[3]) for point in profile_data["geom"]] )
        # Add autosearch mode to all dated profiles: TODO update when mode can be set for each dataset
        profile_data["autosearch"] = autosearch
        new_dated_profiles.append(profile_data)
    transect_data["dated_profiles"] = new_dated_profiles
    logging.debug("Transect data before serializing: %s", transect_data)

    # Use GeoDjango API to convert transect head and axis to appropriate GEOS types for PostGIS
    transect_data["profile_head"] = Point(transect_data["profile_head"][0], transect_data["profile_head"][1])
    transect_data["profile_axis"] = LineString(transect_data["profile_axis"])

    serializer = TransectSerializer(data=transect_data)

    if serializer.is_valid():
        serializer.save()
        return serializer.data
    else:
        logging.error("Transect serializer invalid: %s", serializer.errors)
        raise serializers.ValidationError(serializer.errors)

@shared_task
def delete_datedprofiles(dates_to_delete_list, transect_data):
    '''
    Delete every dated_profile instance from the input list
    Update existing data with new boundaries (for volume computation) accordingly
    '''
    logging.info("Celery task ongoing to remove dated profiles for transect %s", transect_data["uuid"])

    # Manage existing data related to the same transect: compute new reference for dune toe and update volumes computation
    # Compute common window boundaries without the profiles selected for deletion
    original_transect_dp_data = transect_data["dated_profiles"]["features"]
    updated_transect_dp_data = list(filter(lambda d: d['id'] not in dates_to_delete_list, original_transect_dp_data))
    window_low_ref, min_dune_toe_ref, window_high_ref = compute_common_window(updated_transect_dp_data, [])

    # Update original data based on new reference dune toe for volumes computation
    update_dated_profiles_volumes(updated_transect_dp_data, min_dune_toe_ref, window_low_ref, window_high_ref)

    # Delete selected dates
    for date in dates_to_delete_list:
        date_to_delete = DatedProfile.objects.get(uuid=date)
        date_to_delete.delete()