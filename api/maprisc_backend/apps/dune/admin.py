from django.contrib import admin
from django.contrib.gis.geos import LineString
from django.contrib.gis.db import models
from django.contrib.gis import admin as gis_admin

from maprisc_backend.apps.dune.models import Transect, DatedProfile

# Register your models here.

admin.site.register(Transect, gis_admin.OSMGeoAdmin)
admin.site.register(DatedProfile, gis_admin.OSMGeoAdmin)
