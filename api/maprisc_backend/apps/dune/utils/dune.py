import os
import sys
import argparse
import math
import csv
import logging
import time

import pandas as pd
import numpy as np
from scipy.signal import argrelmax, argrelmin, savgol_filter, find_peaks
from scipy.integrate import simps
from scipy.interpolate import interp1d

import matplotlib.pyplot as plt

from django.conf import settings

def gaussian_filter(sigma, window):
    """
    Creates gaussian filter on a specified reference window

    Parameters
    ---------------

    sigma : int
        Standard deviation parameter of the gaussian
    window : string
        List of reference window characteristics as a string
        Ex: "-5,1,5"

    Returns
    ---------------

    Returns gaussian filter to be applied as a convolution
    """
    # Window is an input parameter in the shape "start,step,end"
    bounds = [float(i) for i in window.split(',')]
    # Add 1 to end bound because np.arange exludes end value
    window = np.arange(bounds[0],bounds[2]+1,bounds[1])
    return (1/(sigma*math.sqrt(2*math.pi)))*np.exp(-(window/sigma)**2/2)

def lowpass_filter(profile, lowpass):
    """
    Return a Series for which data have been filtered using low pass filter
    Boundaries are "manually" set to original values

    Parameters
    --------------
    profile : Series
        A pandas Series containing signal to filter
    lowpass : Array
        Array with the filter window
    """

    boundaries = len(lowpass) // 2
    # Numpy convolution between input data and the lowpass filter
    result = np.convolve(profile, lowpass, 'valid')

    return profile[:boundaries].append(pd.Series(result), ignore_index=True).append(profile[-boundaries:], ignore_index=True)

def highpass_filter(profile, highpass):
    """
    Return a Series for which data have been filter using high pass filter
    Boundaries are "manually" set to 0

    Parameters
    --------------
    profile : Series
        A pandas Series containing signal to filter
    
    highpass : Array
        Array with the parameters for the filter
    """
    # Warning : Numpy convolution "flips" the array (see Numpy doc). So we need to flip it first.
    highpass = np.flip(highpass)
    result = np.convolve(profile, highpass, 'valid')
    return pd.Series([0]).append(pd.Series(result), ignore_index=True).append(pd.Series([0]), ignore_index=True)

def find_default_peaks(profile):
    """
    Detection of peaks using find_peaks method from scipy library
    Returns pandas Series with detected points

    Parameters
    --------------
    profile : Series
        A pandas Series containing signal to filter
    """

    # Argrelmax from scipy returns tuple of arrays for each axis.
    # Here, we only need the first and only array
    peaks_indices = find_peaks(profile.values)[0]
    peaks = [value if index in peaks_indices else 0 for (index, value) in enumerate(profile.values)]

    return pd.Series(peaks)

def find_savgol_peaks(profile):
    # Argrelmax from scipy returns tuple of arrays for each axis.
    # Here, we only need the first and only array
    peaks_indices = argrelmax(savgol_filter(profile.values, 5, 2, mode='nearest'))[0]
    peaks = [value if index in peaks_indices else 0 for (index, value) in enumerate(profile.values)]
    
    return pd.Series(peaks)

def find_hollows(profile):
    hollows_indices = argrelmin(profile.values)[0]
    hollows = [value if index in hollows_indices else 0 for index, value in enumerate(profile.values)]
    
    return pd.Series(hollows)

def select_quantile(profile, point_type, threshold):
    """
    Make a selection of highest (absolute) values based on a threshold similar to quantile
    E.g: if threshold is 0.9, keep the 10% highest values
    """
    values = profile.values
    if point_type == "hollows":
        points = values[values < 0]
    else:
        points = values[values > 0]
        
    # Option 1 : keep values greater than (in absolute) the median value
    #median = np.mean(points)
    #highest_points = points[points < median]

    # Option 2 : keep the xth centile
    nb_points_to_keep = int((1 - threshold) * len(points))
    nb_points_to_keep = nb_points_to_keep if nb_points_to_keep > 0 else 1
    logging.info("Select quantile for %s. points: %s, nb_points: %s.", point_type, points, nb_points_to_keep)
    # Sort and slice
    if point_type == "hollows":
        # Sort from lower to higher (negative values)
        highest_points = np.sort(points)[:nb_points_to_keep]
    else:
        # Sort from higher to lower (positive values)
        highest_points = -np.sort(-points)[:nb_points_to_keep]

    selected_points = [value if value in highest_points else 0 for index, value in enumerate(profile.values)]
    logging.info("Quantile selection for %s. selected_points: %s", point_type, selected_points)

    return pd.Series(selected_points)

def profile_needs_resampling(profile, threshold=0.2):
    """
    Check whether profile needs resampling or not.
    Decision is made upon the {number of input points} / {profile length} ratio which should be close to 1 (1 points every meter).
    Threshold is 0.2 (1 point every 5 meters) by default
    """
    profile_indices = profile.loc[:,'point_index']
    profile_length = profile_indices.max() - profile_indices.min()
    profile_points_nb = len(profile.index)
    return (profile_points_nb / profile_length) <= threshold

def profiles_processing(params, input_dataframe, resampling, autosearch=True):
    """ 
    Identify peaks and hollows for crest and foot detection :
    - crest detection is based on signal filtering (either lowpass/highpass sequence or using SavGol)
    - foot detection uses successive lowpass and highpass filters

    Parameters
    ---------------

    - params : dict
        Dictionary of processing parameters
    - input_dataframe : DataFrame
        Input pandas DataFrame. DataFrame structure should be:
        "profile_index | point_index | z | x | y"
    - resampling : Boolean
        Define whether profile needs resampling before processing
    - autosearch : Boolean
        Define which mode is used to compute profile characteristics. Manual mode skips signal processing and only applies resampling.

    Returns
    ---------------

    - profiles : Dataframe
        Dataframe with list of characteristics per profile

    """
    profiles = input_dataframe
    logging.info("Starting processing profiles %s with resampling set at %s", profiles.shape, resampling)

    # Note: resampling (1m) is applied if the input profile doesn't have enough points
    # Otherwise, just rely on profile processing output (peaks and hollows detection)
    if resampling:
        point_indices = profiles["point_index"]
        point_z_values = profiles["z"]
        # Resample starting from lower point to last point (both excluded) with a space of 1 meter
        # Take the closer integer using math.floor() to have comparable points indices (use for elevation comparison)
        # Warning: first interpolated point is 1 meter away from first original point to avoid duplicated points if first index is 0
        x_samples_with_possible_duplicates = np.arange(math.floor(point_indices.iloc[0] + 1), math.ceil(point_indices.iloc[-1] - 1), 1)
        # Remove duplicates if they exist by checking original data
        x_samples = [ index for index in x_samples_with_possible_duplicates if index not in point_indices.values ]
        # Interpolate elevation values based on new samples
        z_samples = np.around(np.interp(x_samples, point_indices, point_z_values), decimals=3)
        # Append new samples to original dataframe
        profiles = profiles.append(
            pd.DataFrame({
                "point_index": x_samples,
                "z": z_samples
            }),
            ignore_index=True
        )
        profiles.sort_values(by=['point_index'], inplace=True, ignore_index=True)

    # Apply signal processing filters if autosearch mode is requested
    if autosearch:
        if resampling:
            # If profile was resampled, use smaller window for filter
            filter_type = params['lowpass_dgps']
        else:
            filter_type = params['lowpass_foot']

        profiles["z_lowpass_foot"] = lowpass_filter(profiles['z'], filter_type)
        profiles["z_lowpass_2_foot"] = lowpass_filter(profiles['z_lowpass_foot'], filter_type)
        profiles["z_highpass_foot"] = highpass_filter(profiles['z_lowpass_2_foot'], params['highpass'])

        profiles["z_lowpass_3_foot"] = lowpass_filter(profiles['z_highpass_foot'], filter_type)
        profiles["z_lowpass_4_foot"] = lowpass_filter(profiles['z_lowpass_3_foot'], filter_type)
        profiles["z_highpass_2_foot"] = highpass_filter(profiles['z_lowpass_4_foot'], params['second_highpass'])

        # Extract peaks and hollows
        profiles["hollows"] = find_hollows(profiles['z_highpass_2_foot'])
        if resampling:
            # If DGPS profile, use elevation ('z' column) instead of filtering output
            profiles["peaks"] = find_default_peaks(profiles['z'])
            # Use quantile selection for crest and toe detection
            profiles["high_peaks"] = select_quantile(profiles['peaks'], "peaks", params['hollow_quantile'])
            profiles["high_hollows"] = select_quantile(profiles['hollows'], "hollows", params['hollow_quantile'])
        else:
            # Otherwise (LiDAR profile), no quantile selection is applied for detection
            profiles["peaks"] = find_default_peaks(profiles['z_highpass_2_foot'])

    logging.info("Done processing profiles.")

    return profiles

def extract_profiles_specs(params, profile, resampling):
    """
    Extracts crest and foot for profile within a Dataframe.
    Crest is the first peak above an altitude threshold.
    Foot is the first hollow preceding the crest.
    Additional heuristics are used if processing DGPS profiles
    """

    # TODO : there is only one profile so profile_index column is useless

    logging.info("Start profile specifications extraction")

    columns = ['crest_index', 'crest_z', 'foot_index', 'foot_z']
    values = [-1 for item in columns]

    if resampling:
        try:
            # First, extract crest based on threshold defined in config
            crest = profile[ (profile['z'] > params["z_th"]) & (profile['high_peaks'] > 0) ].iloc[0]
        except:
            logging.info("Error extracting crest, check input data. Continue picking highest z value")
            crest = profile.loc[profile['z'].idxmax()]
            try:
                # Then, get the foot : first hollow preceding the crest
                foot = profile[ (profile['high_hollows'] < 0) & (profile['point_index'] < crest['point_index']) ].iloc[-1]
                # Add the profile specs to results
                values = [crest['point_index'], crest['z'], foot['point_index'], foot['z']]
            except Exception as error:
                logging.info("Error extracting dune toe. Continue picking first hollow (not in quantile selection) preceding the crest. Message: %s", error)
                try:
                    foot = profile[ (profile['hollows'] < 0) & (profile['point_index'] < crest['point_index']) ].iloc[-1]
                    # Add the profile specs to results
                    values = [crest['point_index'], crest['z'], foot['point_index'], foot['z']]
                except Exception as error:
                    # Crest/Foot extraction failed, probably due to the profile shape (no points above the threshold or no significant peak)
                    # Return None and handle this as an error
                    logging.error("Error extracting crest and foot, check input data: %s", error)
        else:
            foot = profile[ (profile['high_hollows'] < 0) & (profile['point_index'] < crest['point_index']) ].iloc[-1]
            # Add the profile specs to results
            values = [crest['point_index'], crest['z'], foot['point_index'], foot['z']]
    else:
        # LIDAR profile type or enough points in DGPS
        # First, extract crest based on threshold defined in config
        crest = profile[ (profile['z'] > params["z_th"]) & (profile['peaks'] > 0) ].iloc[0]
        # Then, get the foot : first hollow preceding the crest
        foot = profile[ (profile['hollows'] < 0) & (profile['point_index'] < crest['point_index']) ].iloc[-1]
        # Add the profile specs to results
        values = [crest['point_index'], crest['z'], foot['point_index'], foot['z']]
    
    profile_specs = pd.DataFrame([values], columns=columns)

    logging.info("Done extracting profile specifications")
    logging.debug("Profile crest and feet results overview : %s", profile_specs.head())

    return profile_specs


def retrieve_manually_set_profile_specs(input_dataframe, crest_code="CDD", toe_code="PDD"):
    """ 
    Retrieve dune crest and toe based on user defined points.
    Point type is defined as 'type' in each GeoJSON feature 'properties'.

    Parameters
    ---------------

    input_dataframe: DataFrame
        Input pandas DataFrame. DataFrame structure should be:
        "point_index | x | y | z | point_type"
    crest_code: String
        User defined value in input data to identify dune crest. Default is `CDD`
    toe_code: String
        User defined value in input data to identify dune toe. Default is `PDD`

    Returns
    ---------------

    profiles_specs: DataFrame
        DataFrame with profile characteristics (crest and toe)

    """
    logging.info("Start manual extraction of profile characteristics")
    columns = ['crest_index', 'crest_z', 'foot_index', 'foot_z']

    # Selecting row corresponding to the dune crest
    crest_df = input_dataframe.loc[input_dataframe['point_type'] == crest_code]
    # Selecting row corresponding to the dune toe
    toe_df = input_dataframe.loc[input_dataframe['point_type'] == toe_code]

    # Add the profile crest and toe to the results
    values = [crest_df['point_index'].values[0], crest_df['z'].values[0], toe_df['point_index'].values[0], toe_df['z'].values[0]]

    profile_specs = pd.DataFrame([values], columns=columns)

    logging.info("Done extracting profile characteristics")
    logging.debug("Profile crest and feet results overview : %s", profile_specs.head())

    return profile_specs

def compute_surface(profile, x_vector=[], spacing_unit=1, method="simps"):
    """ 
    Computes surface under a curve composed of points.
    Based on nump.trapz or scipy.integrate.simps
    Simpson's rule typically gives far superior results for numerical integration, compared to the trapezoidal rule

    Parameters
    ---------------

    profile : list
        List of points composing the curve
    x_vector: list
        List of point positions along the profile
    spacing_unit : integer
        Units (e.g. meters) between points
    method : str
        Name of the method to be used : simps or trapz
    Returns
    ---------------

    surface : float
        Estimation of the area under the curve
    """
    surface = 0
    if method == "trapz":
        surface = np.trapz(profile, dx=spacing_unit)
    else:
        if len(x_vector) > 0:
            surface = simps(profile, x=x_vector)
        else:
            surface = simps(profile, dx=spacing_unit)
    return surface

def compute_volume(profile, x_vector, spacing_unit, method="simps"):
    """ 
    Computes linear volume (m3/m.l) as a normalization unit.
    V = surface (considering a virtual 1m distance along X axis)

    Parameters
    ---------------

    profile : list
        List of points composing the curve
    x_vector: list
        List of point positions along the profile
    spacing_unit : integer
        Units (e.g. meters) between points
    method : str
        Name of the method to be used : simps or trapz
    Returns
    ---------------

    surface : float
        Estimation of the linear volume under the curve, with 3 decimals
    """
    volume = 0

    # applies scipy method based on provided argument
    if method == "trapz":
        surface = compute_surface(profile, x_vector, spacing_unit, method="trapz")
    else:
        surface = compute_surface(profile, x_vector, spacing_unit)
    volume = surface
    return round(volume, 3)

def compute_profile_volume_uncertainty(input_profile, uncertainty):
    """ 
    Compute standard deviation from 1D profile considering evenly spaced intervals.
    Intervals are theoretically unevenly spaced (due to original data points), but profiles are resampled so:
    - most of the intervals are actually even (1m each)
    - other intervals are extremely small (10-3 m as an order of magnitude), leading to absurd values of uncertainty (because of the division by the length of intervals in Simpson composite rule)
    Statements :
    - We assume that Simpson's rule is used for integration approximation according to https://en.wikipedia.org/wiki/Simpson%27s_rule#Composite_Simpson%27s_rule_for_irregularly_spaced_data
    - We assume that initial positioning error is constant among all profile points
    - We assume the number of points is odd (thus the number of intervals is even)
    Computation propagates variance in a weighted sum, as stated in this SO discussion : https://stats.stackexchange.com/questions/214850/propagate-errors-in-measured-points-to-simpsons-numerical-integral

    Parameters
    ---------------

    profile : DataFrame
        Input pandas DataFrame. DataFrame structure should be:
        "point_index | z | x | y"
    uncertainty : Float
        Uncertainty on point elevation. Note: this uncertainty is the same for every point.

    Returns
    ---------------

    uncertainty : Float
        Estimated standard deviation of approximate integration on input profile using Simpson's rule. Rounds result to 3 decimals

    """

    intervals_number = len(input_profile.index) - 1
    points_number = len(input_profile.index)

    delta_x = (input_profile.iloc[-1]['point_index'] - input_profile.iloc[0]['point_index']) / intervals_number
    variance = uncertainty**2 * delta_x**2 * (2 + 20 * (points_number - 2) / 2) / 9
    return round(math.sqrt(variance), 3)

def plot_profile(data, x_value_label, y_value_labels, y_bis_value_labels, export, params, profile_specs=None):
    """
    Plot data using matplotlib
    ** Used in development environment only **
    """

    logging.info("Preparing plot…")

    fig, (ax1,ax2) = plt.subplots(2, 1)
    fig.set_size_inches(12,8)
    fig.set_dpi(150)
    data.plot(ax=ax1, x=x_value_label, y=y_bis_value_labels, kind="line", grid=True)
    data.plot(ax=ax2, x=x_value_label, y=y_value_labels, kind="line", grid=True)
    if profile_specs is not None:
        # Retrieve crest_x and foot_x with a join between dataframes
        crest_x = profile_specs["crest_index"].iloc[0]
        foot_x = profile_specs['foot_index'].iloc[0]

        # Check if crest was effectively found
        if crest_x != -1:
            plt.axvline(x=crest_x, color="red", linestyle="dashed", label="Dune crest")
            plt.axvline(x=foot_x, color="green", linestyle="dashed", label="Dune foot")

        plt.legend()
    else:
        pass
    fig.savefig(os.path.join("/app/", f'{time.time()}.png'), dpi=fig.dpi)

def export_csv_data(data, output_filename):
    '''
    Exports processing info as csv output for analytics
    ** Used in development environment only **
    '''
    logging.info("Export results as csv…")
    
    success = False
    try:
        data.to_csv(os.path.join('/app/', f'{output_filename}_{time.time()}.csv'), index=None, header=True)
        success = True
    except Exception as error:
        logging.error("CSV export failed. Error message: ", error)
    return success


def handle_transect_points(geometry_data, toe_code, crest_code, uncertainty, phma_site, autosearch=True):
    '''
    Process input GeoJSON data to be stored in database

    Parameters
    ---------------

    geometry_data: GeoJSON formatted data
        List of X,Y,Z points
    toe_code: String
        Code used to define the middle morphological marker. Not used if autosearch mode is activated.
    crest_code: String
        Code used to define the upper morphological marker. Not used if autosearch mode is activated.
    uncertainty: Float
        Relative error in centimeters on positioning points elevation
    autosearch: Boolean
        Define which mode is used to compute profile characteristics : either automatic using signal processing or manual if based on user input
    phma_site : Float
        Highest astronomical sea for related site in meters

    Returns
    ---------------

    profile_data : dict
        Object with profile data like so:
        {
            "crest_index": XX,
            "foot_index": YY,
            "uncertainty": KK,
            "geom": […],
            "volumes": […]
        }
    '''

    # Only one layer in the geojson file
    feature_list = [feature for feature in geometry_data["features"]]

    # If autosearch is False, append point type for further extraction
    if not autosearch:
        point_geom_list = [ [feat["properties"]["distance"], feat["geometry"]["coordinates"][0], feat["geometry"]["coordinates"][1], feat["geometry"]["coordinates"][2], feat["properties"]["CODE"], ] for feat in feature_list]
        input_df = pd.DataFrame(point_geom_list, columns=['point_index', 'x', 'y', 'z', 'point_type'])
    else:
        point_geom_list = [ [feat["properties"]["distance"], feat["geometry"]["coordinates"][0], feat["geometry"]["coordinates"][1], feat["geometry"]["coordinates"][2]] for feat in feature_list]
        input_df = pd.DataFrame(point_geom_list, columns=['point_index', 'x', 'y', 'z'])

    # Extract profile characteristics
    params = {
        'lowpass_foot': [0.11, 0.22, 0.34, 0.22, 0.11],
        'lowpass_dgps': [0.25, 0.50, 0.25],
        'highpass': [-1, 0, 1],
        'second_highpass': [1, 0, -1],
        'z_th': phma_site,
        'hollow_quantile': 0.85,
        'resampling_threshold': 0.2,
    }

    #################################
    # Core of the process
    #################################


    # Force resampling to be sure profiles get common point indices
    params["resampling_threshold"] = 1
    resampling = profile_needs_resampling(input_df, params["resampling_threshold"])

    # Then, process input profile as a 1D signal by applying a sequence of filters to identify peaks and hollows
    profile = profiles_processing(params, input_df, resampling, autosearch)
    # Check whether user chose manual or automatic identification of profile characteristics
    if autosearch:
        # Second, use filters result to extract dune crest and toe based on heuristics
        profile_specs = extract_profiles_specs(params, profile, resampling)
    else:
        profile_specs = retrieve_manually_set_profile_specs(profile, crest_code=crest_code, toe_code=toe_code)
    
    # Export as csv if Django DEBUG settings is set at True
    if settings.DEBUG:
        export_csv_data(profile, 'profile_data')
        export_csv_data(profile_specs, 'profile_specs_data')

    #################################
    # Profile computation end
    logging.info("Results for current profile: %s", profile_specs)
    #################################

    # Optional: plot profile to help investigate the process
    # WARNING Plots are saved on disk so either clean the files with a cron task or comment this part if not useful, especially in production
    if settings.DEBUG:
        logging.info("Plotting crest/toe identification only if autosearch mode activated. Autosearch mode is set at %s", autosearch)
        if autosearch:
            if resampling:
                plot_profile(profile, "point_index", ["z", "peaks", "high_peaks"], ["z_highpass_2_foot", "hollows", "high_hollows"], False, params, profile_specs)
            else:
                plot_profile(profile, "point_index", ["z", "peaks"], ["z_highpass_2_foot", "hollows"], False, params, profile_specs)

    # Create explicit variables for profile characteristics
    if profile_specs.iloc[0]['crest_index'] != -1:
        crest_index = profile_specs.iloc[0]['crest_index']
        crest_z = profile_specs.iloc[0]['crest_z']
        foot_index = profile_specs.iloc[0]['foot_index']
        foot_z = profile_specs.iloc[0]['foot_z']
    else:
        # Extraction failed, use -1 as an error value
        crest_index = -1
        foot_index = -1
        crest_z = -1
        foot_z = -1

    # Return data to be stored in database
    # Original points with geographical data are stored in "geom"
    # All points (coming from resampling) are stored in "points_position"
    points_position = profile[['point_index', 'z']].values.tolist()
    profile_data = {
        "crest_index": crest_index,
        "foot_index": foot_index,
        "geom": point_geom_list,
        "uncertainty": uncertainty,
        "points_position": points_position,
        "volumes": []
    }

    return profile_data

def linear_interpolation_1d(point_list, index_reference):
    """
        Find approximate value (using interp1d)
        Use the first point in the profile whose index precedes the reference in order to interpolate

        Parameters
        ---------------

        point_list : list
            List of (index, z) points: [(0, 1.5), (1, 2.3), …]
        index_reference : float
            index of the point for which to interpolate z value

        Returns
        ---------------

        interpolated z value : float
    """
    logging.debug(f"Point list: {point_list}, Index: {index_reference}")
    previous_point = [p for p in point_list if p[0] <= index_reference][-1]
    next_point = [p for p in point_list if p[0] >= index_reference][0]
    interp_function = interp1d([previous_point[0], next_point[0]], [previous_point[1], next_point[1]])

    return interp_function(index_reference)

def unique(sequence):
    '''
    Remove duplicates and preserve order from a given list
    '''
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]

def compute_profile_volumes(profile_data, min_dune_toe_ref, window_low_ref, window_high_ref):
    """
    Compute sand volumes for different stages in the profile within a common window for sake of comparison.
    Use lowest dune toe among dated profiles as a common boundary between shore and dune.
    """
    points_position = profile_data["points_position"]

    # Define boundaries to compute volumes on same window for all dates
    boundaries = [window_high_ref, window_low_ref, min_dune_toe_ref]

    # IMPORTANT: Add dune/shore boundary value to get continuous volumes.
    # Avoid "blank" area that would not be computed in dune and shore windows but computed in global window, thus leading to differences in final values.
    # Repeat for low and high boundary to avoid inconsistent windows (and consequently, volumes)
    for boundary_index in boundaries:
        if boundary_index not in [point[0] for point in points_position]:
            logging.info("Need to interpolate value for boundary estimate. Boundary index: %s", boundary_index)
            interpolated_boundary_value = round(linear_interpolation_1d(points_position, boundary_index).item(), 3)
            points_position.append([boundary_index, interpolated_boundary_value])
            points_position.sort(key=lambda point: point[0])

    # TODO Get ref values from input data instead of hardcoded values
    DISTANCE_BETWEEN_POINTS = 1 # Unit = meters
    POINT_Z_ERROR = profile_data["uncertainty"] / 100 # Unit = meters

    REFS = {
        "LP": ("Lower point", window_low_ref),
        "DC": ("Dune Crest", window_high_ref),
        "DF": ("Dune Foot", min_dune_toe_ref),
    }
    
    volume_list = []

    # IMPORTANT: check if one of the dune toes could not be extracted otherwise compute volume on the entire window only
    if min_dune_toe_ref != -1:
        volume_list += [
            (REFS["DF"], REFS["DC"]),
            (REFS["LP"], REFS["DC"]),
            (REFS["LP"], REFS["DF"]),
        ]
    else:
        volume_list.append(
            (REFS["LP"], REFS["DC"]),
        )

    for window in volume_list:
        min_name, min_index = window[0]
        max_name, max_index = window[1]

        # Warning: Profiles are discrete so boundaries may not have corresponding values in every profiles
        try:
            # Only use points within the common window
            low_boundary_index = max(min_index, window_low_ref)
            high_boundary_index = min(max_index, window_high_ref)
            points = [(p[1], p[0]) for p in points_position if p[0] >= low_boundary_index and p[0] <= high_boundary_index]
            # Remove possible duplicates in original points (identical coordinates or separated from less than 1mm) BUT preserve order (Python set() does not because set is an unordered data structure)
            points = unique(points)
        except Exception as error:
            logging.error("Error when selecting profile section for %s;%s with boundaries %s;%s. Error message: %s", min_name, max_name, min_index, max_index, error)
        else:
            if len(points) > 0:
                volume = compute_volume([p[0] for p in points], [p[1] for p in points], DISTANCE_BETWEEN_POINTS)
                uncertainty = compute_profile_volume_uncertainty(pd.DataFrame(points, columns=['z', 'point_index']), POINT_Z_ERROR)
                logging.info("Volume and uncertainty computed: %s, %s, %s", volume, uncertainty, POINT_Z_ERROR)
                if not volume or np.isnan(volume):
                    # Do not add None value as it won't be supported by Postgresql nor Django REST JSON serializer
                    logging.error("Error when computing volume for %s;%s with window %s:%s. Points: %s.", min_name, max_name, low_boundary_index, high_boundary_index, points)
                else:
                    window_volume_data = {
                        "min_ref": min_name,
                        "min_index": low_boundary_index,
                        "max_index": high_boundary_index,
                        "max_ref": max_name,
                        "volume": volume,
                        "uncertainty": uncertainty,
                    }
                    profile_data["volumes"].append(window_volume_data)
                    logging.info("Points used for volume : %s and volume results %s with boundaries (%s,%s,%s,%s)", points, volume, min_index, low_boundary_index, max_index, high_boundary_index)
    logging.info("Finished computing sand budgets on input profiles: %s", profile_data["volumes"])

    return profile_data