from shapely.geometry import Point, LineString
from shapely.ops import snap

def project_point_along_line(point: Point, profile_axis: tuple) -> Point:
    '''
    Snap raw point to profile axis

    Parameters
    ---------------
    point: Point
        Shapely point object
    profile_axis: tuple
        Tuple of point coordinates in meters for axis line ((X0, Y0), (X1, Y1))

    Returns
    ---------------
    Returns Shapely Point object snapped to profile axis
    '''

    axis_line = LineString(profile_axis)
    return snap(point, axis_line, 1)

def compute_distance_to_profile_head(point: Point, profile_head: tuple) -> float:
    '''
    Compute distance between a point and the profile head, with 3 decimals.

    Parameters
    ---------------
    point: Point
        Shapely point object
    profile_head: tuple
        Tuple of point coordinates in meters for axis line (X0, Y0)

    Returns
    ---------------
    Returns Shapely Point object snapped to profile axis
    '''
    head = Point(profile_head)
    return round(head.distance(point), 3)

def update_geojson_profile_with_distance(profile: dict, profile_head: list, profile_axis: list) -> dict:
    '''
    Main preprocessing function to update input geojson profile with computed distance field for each point.
    Remove duplicates based on point distance to avoid division by zero during volume computation (due to simpson integration method).
    
    Parameters
    ---------------
    profile_head: List
        List with profile head coordinates in meters
    profile_axis: List
        List of points coordinates in meters composing profile axis line
    profile: dict
        Dictionary with date, uncertainty and geojson keys

    Returns
    ---------------
    Returns updated profile with new property (i.e. computed distance to profile head) for each point
    '''

    # Check for duplicates during process
    seen_indices = []
    updated_features = []

    for point in profile["geojson"]["features"]:
        shapely_point = Point(point["geometry"]["coordinates"])
        projected_point = project_point_along_line(shapely_point, profile_axis)
        distance = compute_distance_to_profile_head(projected_point, profile_head)
        # Remove point if it is at the same distance than one other point
        if distance not in seen_indices:
            point["properties"]["distance"] = distance
            updated_features.append(point)
            seen_indices.append(distance)
    profile["geojson"]["features"] = updated_features
    
    return profile