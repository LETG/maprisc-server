# Generated by Django 3.0.5 on 2022-04-13 14:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dune', '0019_transect_distance_to_profile_head'),
    ]

    operations = [
        migrations.RenameField(
            model_name='transect',
            old_name='proxile_axis',
            new_name='profile_axis',
        ),
    ]
