# Generated by Django 3.0.5 on 2020-04-15 12:47

import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Transect',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='created')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='updated')),
                ('is_active', models.BooleanField(db_index=True, default=True)),
                ('index', models.IntegerField()),
                ('name', models.CharField(blank=True, max_length=100, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DatedProfile',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='created')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='updated')),
                ('is_active', models.BooleanField(db_index=True, default=True)),
                ('date', models.DateTimeField()),
                ('crest_index', models.IntegerField()),
                ('foot_index', models.IntegerField()),
                ('geom', django.contrib.gis.db.models.fields.MultiPointField(blank=True, dim=3, null=True, srid=2154)),
                ('transect', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='dune.Transect')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
