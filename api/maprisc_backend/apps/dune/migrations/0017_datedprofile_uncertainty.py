# Generated by Django 3.0.5 on 2022-02-07 16:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dune', '0016_auto_20220131_1542'),
    ]

    operations = [
        migrations.AddField(
            model_name='datedprofile',
            name='uncertainty',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
    ]
