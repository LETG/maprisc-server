from django.urls import reverse
from django.contrib.gis.geos import Point, MultiPoint, LineString

from rest_framework import status

import pytest
import os
import json
from collections import OrderedDict

from maprisc_backend.apps.dune.models import Transect, DatedProfile
from maprisc_backend.apps.dune.api.v1.tasks import process_datedprofiles
from maprisc_backend.apps.dune.api.v1.serializers.dune_profile import TransectSerializer
from maprisc_backend.apps.common.models import UserAction

THIS_DIR = os.path.dirname(os.path.abspath(__file__))

@pytest.fixture
def example_profile_w_two_dates():
    # Use OMDM PR02 DGPS data
    t_profile_head = [308217.3118, 6655976.788]
    t_profile_axis = [308173.7289, 6655826.0181], [308442.505099997, 6656755.8168]

    # Create dict content from geojson file
    profile_1 = os.path.join(THIS_DIR, 'DATA/OMDM-PR02-DGPS-2019-09-30.geojson')
    profile_2 = os.path.join(THIS_DIR, 'DATA/OMDM-PR02-DGPS-2020-07-23.geojson')
    with open(profile_1) as profile_1_geojson:
        profile_1_content = json.load(profile_1_geojson)
        with open(profile_2) as profile_2_geojson:
            profile_2_content = json.load(profile_2_geojson)
            dated_profiles_data = [
                {
                    'date': "2019-09-30",
                    'uncertainty': 5,
                    'geojson': profile_1_content,
                },
                {
                    'date': "2020-07-23",
                    'uncertainty': 5,
                    'geojson': profile_2_content,
                }
            ] # nosec
    
    # Create transect with dated profiles
    transect_data = {
        "index": 2,
        "name" : "OMDM",
        "phma" : 4,
        "r1": "PDD",
        "r2": "CDD",
        "profile_head": t_profile_head,
        "profile_axis": t_profile_axis,
        "autosearch" : False,
        "dated_profiles": dated_profiles_data
    }

    return transect_data

@pytest.fixture
def example_transect_object(example_profile_w_two_dates):
    return Transect(
        index=example_profile_w_two_dates["index"],
        name=example_profile_w_two_dates["name"],
        autosearch=example_profile_w_two_dates["autosearch"],
        phma=example_profile_w_two_dates["phma"],
        profile_head=Point(example_profile_w_two_dates["profile_head"]),
        profile_axis=LineString(example_profile_w_two_dates["profile_axis"]),
        distance_to_profile_head=115.525,
    )

@pytest.mark.django_db
def test_get_transect_api_list_empty(api_client):
    """ Random user has no associated transect"""

    url = reverse("api-v1-dune:transect-list")
    client = api_client()
    response = client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()["results"]) == 0

@pytest.mark.django_db
def test_get_transect_api_list_unauthorized(unauthorized_api_client):
    """ Unauthorized request should fail """

    url = reverse("api-v1-dune:transect-list")
    response = unauthorized_api_client.get(url)

    assert response.status_code == status.HTTP_401_UNAUTHORIZED

@pytest.mark.django_db
def test_create_transect_api_success(api_client):
    """ Empty transect processing is synchronous. Check created properties """
    assert Transect.objects.count() == 0

    # WARNING processing tasks wont' be called due to empty dated profile list
    # Thus, transect serializer will be applied as-is, so profile head and axis should be geometry compliant values
    data = {
        "index": 1,
        "phma": 4,
        "autosearch": True,
        "profile_head": [1, 1],
        "profile_axis": [[0,0], [1,1]],
        "name": "Transect #1",
    }  # nosec
    client = api_client()
    response = client.post(reverse("api-v1-dune:transect-list"), data)

    assert response.status_code == status.HTTP_201_CREATED # Status code should be 201 because transect is empty (no dated profile) so it is saved synchronously
    assert Transect.objects.count() == 1
    assert response.data['index'] == data["index"]
    assert response.data['name'] == data["name"]
    assert response.data['phma'] == data["phma"]
    assert response.data['profile_head']['coordinates'] == data["profile_head"] # response.data['profile_head'] returns GeoJSON Dict
    assert response.data['profile_axis']['coordinates'] == data["profile_axis"]
    assert response.data['distance_to_profile_head'] == None # Transect was created with empty data
    assert response.data['dated_profiles'] == OrderedDict([('type', 'FeatureCollection'), ('features', [])])


@pytest.mark.django_db
def test_get_transect_api_list(api_client):
    """
    Test list of returned transects filtered by the authenticated user
    """
    
    # WARNING processing tasks wont' be called due to empty dated profile list
    # Thus, transect serializer will be applied as-is, so profile head and axis should be geometry compliant values
    data = {
        "index": 1,
        "phma": 4,
        "autosearch": True,
        "profile_head": [1, 1],
        "profile_axis": [[0,0], [1,1]],
        "name": "Transect #1",
    }  # nosec

    client = api_client()
    create_response = client.post(reverse("api-v1-dune:transect-list"), data)
    assert create_response.status_code == status.HTTP_201_CREATED

    read_response = client.get(reverse("api-v1-dune:transect-list"))
    assert read_response.status_code == status.HTTP_200_OK
    assert len(read_response.json()["results"]) == 1
    assert read_response.json()["results"][0]["name"] == data["name"]

    # Check that another user (new client) doesn't see the same list
    new_client = api_client()
    new_response = new_client.get(reverse("api-v1-dune:transect-list"))
    assert new_response.status_code == status.HTTP_200_OK
    assert len(new_response.json()["results"]) == 0

@pytest.mark.django_db
def test_get_transect_api_detail_authorized(api_client):
    """
    Test access to transect details filtered by the authenticated user
    """
    
    # Manually create a transect to explicitly declare the user
    # WARNING processing tasks wont' be called due to empty dated profile list
    # Thus, transect serializer will be applied as-is, so profile head and axis should be geometry compliant values
    data = {
        "index": 1,
        "phma": 4,
        "autosearch": True,
        "profile_head": [1, 1],
        "profile_axis": [[0,0], [1,1]],
        "name": "Transect #1",
    }  # nosec

    client = api_client()
    create_response = client.post(reverse("api-v1-dune:transect-list"), data)
    assert create_response.status_code == status.HTTP_201_CREATED
    
    response_data = create_response.json()
    read_response = client.get(reverse("api-v1-dune:transect-detail", args=[response_data["uuid"]]))
    assert read_response.status_code == status.HTTP_200_OK

@pytest.mark.django_db
def test_get_transect_api_detail_unauthorized(dune_transect, api_client):
    """
    Test permission denied to transect details if user does not match
    """
    # Create a transect
    # WARNING : dune_transect fixture is using a temporary user

    # WARNING Bakery is expecting a SpatialProxy compliant type, using django GEOS
    t_index = 1
    t_name = "Transect #1"
    t_phma = 4
    t_profile_head = Point([1, 1])
    t_profile_axis = LineString([0,0], [1,1])
    t_autosearch = True
    transect = dune_transect(index=t_index, phma=t_phma, name=t_name, autosearch=t_autosearch,
                        profile_head=t_profile_head, profile_axis=t_profile_axis)

    # Use another client to check if permission is denied
    client = api_client()
    read_response = client.get(reverse("api-v1-dune:transect-detail", args=[transect.uuid]))
    assert read_response.status_code == status.HTTP_403_FORBIDDEN

@pytest.mark.django_db
def test_get_datedprofile_api_list(dune_transect, dune_datedprofile, api_client):

    # Create a transect
    t_index = 1
    t_name = "Transect #1"
    t_phma = 4
    t_profile_head = Point([1, 1])
    t_profile_axis = LineString([0,0], [1,1])
    t_autosearch = True
    transect = dune_transect(index=t_index, phma=t_phma, name=t_name, autosearch=t_autosearch,
                        profile_head=t_profile_head, profile_axis=t_profile_axis)

    # Create dated profiles
    number_of_dp_to_create = 2
    for i in range(number_of_dp_to_create):
        year = str(2020 + i)
        dp_date = year + "-01-01 00:00:00.000000+0100"
        dp_crest_index = 1
        dp_foot_index = 1
        dp_uncertainty = 5
        dp_geom = MultiPoint(Point(0, 0, 0), Point(1, 1, 1))
        dated_profile = dune_datedprofile(date=dp_date, crest_index=dp_crest_index, 
                                            foot_index=dp_foot_index, uncertainty=dp_uncertainty, geom=dp_geom,
                                            transect=transect)

    url = reverse('api-v1-dune:datedprofile-list', args=[transect.uuid])
    client = api_client()
    response = client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()['features']) == number_of_dp_to_create

@pytest.mark.django_db
def test_create_datedprofile_api_success(api_client, example_transect_object):
    
    example_transect_object.save()
    assert Transect.objects.count() == 1
    transect = Transect.objects.all().first()

    # Create dict content from geojson file
    new_profile = os.path.join(THIS_DIR, 'DATA/OMDM-PR02-DGPS-2022-04-19.geojson')
    with open(new_profile) as profile_3_file:
        profile_3_geojson = json.load(profile_3_file)
        data = {
            "r1": "PDD",
            "r2": "CDD",
            "dated_profiles": [{
                'date': "2022-04-19",
                'uncertainty': 5,
                'geojson': profile_3_geojson,
            }]
        }  # nosec

    url = reverse("api-v1-dune:datedprofile-list", args=[transect.uuid])
    client = api_client()
    response = client.post(url, data)

    assert response.status_code == status.HTTP_202_ACCEPTED
    assert response.data["status"] == "PENDING"
    assert response.data["task_id"] != ""

    assert UserAction.objects.count() == 1
    action = UserAction.objects.last()
    assert action.action_url == "/api/v1/dune/transects/" + str(transect.uuid) + "/profiles"
    assert action.additional_info == "{{'transect': {}, 'submitted_dates': {}}}".format(transect.uuid, 1)

@pytest.mark.django_db
def test_create_datedprofile_async_processing(example_transect_object):
    
    # Actually create transect object
    example_transect_object.save()
    assert Transect.objects.count() == 1
    transect = Transect.objects.all().first()
    transect_data = TransectSerializer(transect).data

    # Create dict content from geojson file
    new_profile = os.path.join(THIS_DIR, 'DATA/OMDM-PR02-DGPS-2022-04-19.geojson')
    with open(new_profile) as profile_3_file:
        profile_3_geojson = json.load(profile_3_file)
        data = {
                'r1': "PDD",
                'r2': "CDD",
                "dated_profiles": [
                    {
                        'date': "2022-04-19",
                        'uncertainty': 5,
                        'geojson': profile_3_geojson,
                    }
                ]
        } # nosec

    result = process_datedprofiles(data, transect_data)
    assert len(result["features"]) == 1
    assert result["features"][0]["properties"]["date"].split('T')[0] == data["dated_profiles"][0]["date"]

    
@pytest.mark.django_db
def test_create_nested_transect_api_success(api_client, example_profile_w_two_dates):
    
    client = api_client()
    url = reverse("api-v1-dune:transect-list")
    response = client.post(url, example_profile_w_two_dates)

    assert response.status_code == status.HTTP_202_ACCEPTED
    assert response.data["status"] == "PENDING"
    assert response.data["task_id"] != ""

    # assert action has been recorded
    assert UserAction.objects.count() == 1
    action = UserAction.objects.first()
    assert action.action_url == "/api/v1/dune/transects/"
    assert action.additional_info == "{'submitted_dates':"+str(len(example_profile_w_two_dates['dated_profiles']))+"}"
    assert action.http_request_type == "POST"

@pytest.mark.django_db
def test_create_transect_wo_head_api_error(api_client):
    
    # Create a transect without profile head
    t_index = 1
    t_phma = 4
    t_name = "Transect #1"
    t_profile_head = None
    t_profile_axis = [[307990.482808, 6651285.0040199999], t_profile_head]
    t_autosearch = True

    transect_data = {
        "index": t_index,
        "name": t_name,
        "phma": t_phma,
        "profile_head": t_profile_head,
        "profile_axis": t_profile_axis,
        "autosearch" : t_autosearch,
        "dated_profiles": "[]"
    }
    client = api_client()
    url = reverse("api-v1-dune:transect-list")
    response = client.post(url, transect_data)

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data.startswith("No data provided for profile head")

@pytest.mark.django_db
def test_create_transect_wo_axis_api_error(api_client):
    
    # Create a transect without profile head
    t_index = 1
    t_phma = 4
    t_name = "Transect #1"
    t_profile_head = [308275.49714200001, 6651231.4612400001]
    t_profile_axis = None
    t_autosearch = True

    transect_data = {
        "index": t_index,
        "name": t_name,
        "phma": t_phma,
        "profile_head": t_profile_head,
        "profile_axis": t_profile_axis,
        "autosearch" : t_autosearch,
        "dated_profiles": "[]"
    }
    client = api_client()
    url = reverse("api-v1-dune:transect-list")
    response = client.post(url, transect_data)

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data.startswith("No data provided for profile head")

@pytest.mark.django_db
def test_create_nested_malformed_transect_api_error(api_client):
    
    # Create a transect
    t_index = 1
    t_phma = 4
    t_name = "Transect #1"
    t_profile_head = [308275.49714200001, 6651231.4612400001]
    t_profile_axis = [[307990.482808, 6651285.0040199999], t_profile_head]
    t_autosearch = True

    transect_data = {
        "index": t_index,
        "name": t_name,
        "phma": t_phma,
        "profile_head": t_profile_head,
        "profile_axis": t_profile_axis,
        "autosearch" : t_autosearch,
        "dated_profiles": "[]"
    }
    client = api_client()
    url = reverse("api-v1-dune:transect-list")
    response = client.post(url, transect_data)

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data == "Dated profiles property should be a list not str"

@pytest.mark.django_db
def test_create_nested_transect_success_auto_mode(api_client):
    
    # Create a transect
    t_index = 1
    t_phma = 4
    t_profile_head = [308275.49714200001, 6651231.4612400001]
    t_profile_axis = [[307990.482808, 6651285.0040199999], t_profile_head]
    t_name = "Transect #1"
    t_autosearch = True

    # Create dict content from geojson file
    test_file_path = os.path.join(THIS_DIR, 'test_data.geojson')
    with open(test_file_path) as geojson_file:
        content = json.load(geojson_file)
        data = {
            'date': "2020-01-01",
            'uncertainty': 5,
            'geojson': content,
        }  # nosec

    transect_data = {
        "index": t_index,
        "name": t_name,
        "phma": t_phma,
        "autosearch": t_autosearch,
        "profile_head": t_profile_head,
        "profile_axis": t_profile_axis,
        "dated_profiles": [
            data
        ]
    }
    client = api_client()
    url = reverse("api-v1-dune:transect-list")
    response = client.post(url, transect_data)

    assert response.status_code == status.HTTP_202_ACCEPTED
    assert response.data["status"] == "PENDING"
    assert response.data["task_id"] != ""

    # assert action has been recorded
    assert UserAction.objects.count() == 1
    action = UserAction.objects.first()
    assert action.action_url == "/api/v1/dune/transects/"
    assert action.additional_info == "{'submitted_dates':"+str(len(transect_data['dated_profiles']))+"}"
    assert action.http_request_type == "POST"

@pytest.mark.django_db
def test_create_1D_profile_bad_request(api_client):
    
    # Create a transect
    t_index = 1
    t_phma = 4
    t_name = "Transect #1"
    t_profile_head = [308275.49714200001, 6651231.4612400001]
    t_profile_axis = [[307990.482808, 6651285.0040199999], t_profile_head]
    t_autosearch = False

    transect_data = {
        "index": t_index,
        "name": t_name,
        "phma": t_phma,
        "profile_head": t_profile_head,
        "profile_axis": t_profile_axis,
        "autosearch": t_autosearch,
        "dated_profiles": "[]"
    }
    client = api_client()
    url = reverse("api-v1-dune:transect-list")
    response = client.post(url, transect_data)

    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_create_1D_profile_automatic_search(api_client):
    
    # Create a transect
    t_index = 1
    t_phma = 4
    t_name = "Transect #1"
    t_profile_head = [308275.49714200001, 6651231.4612400001]
    t_profile_axis = [[307990.482808, 6651285.0040199999], t_profile_head]
    t_autosearch = True

    transect_data = {
        "index": t_index,
        "name": t_name,
        "phma": t_phma,
        "profile_head": t_profile_head,
        "profile_axis": t_profile_axis,
        "autosearch": t_autosearch,
        "dated_profiles": "[]"
    }
    client = api_client()
    url = reverse("api-v1-dune:transect-list")
    response = client.post(url, transect_data)

    assert response.status_code == status.HTTP_400_BAD_REQUEST

@pytest.mark.django_db
def test_create_1D_profile_manual_search(api_client):
    
    # Create a transect
    t_index = 1
    t_phma = 4
    t_name = "Transect #1"
    t_profile_head = [308275.49714200001, 6651231.4612400001]
    t_profile_axis = [[307990.482808, 6651285.0040199999], t_profile_head]
    t_autosearch = False

    transect_data = {
        "index": t_index,
        "name": t_name,
        "phma": t_phma,
        "profile_head": t_profile_head,
        "profile_axis": t_profile_axis,
        "autosearch": t_autosearch,
        "dated_profiles": "[]"
    }
    client = api_client()
    url = reverse("api-v1-dune:transect-list")
    response = client.post(url, transect_data)

    assert response.status_code == status.HTTP_400_BAD_REQUEST

@pytest.mark.django_db
def test_flower_api(api_client, dune_transect):
    
    # Create a transect
    t_index = 1
    t_name = "Transect #1"
    t_phma = 4
    t_profile_head = Point([308275.49714200001, 6651231.4612400001])
    t_profile_axis = LineString([307990.482808, 6651285.0040199999], t_profile_head)
    t_autosearch = True
    transect = dune_transect(index=t_index, phma=t_phma, name=t_name, autosearch=t_autosearch,
                        profile_head=t_profile_head, profile_axis=t_profile_axis)

    assert DatedProfile.objects.filter(transect=transect.uuid).count() == 0

    # Create dict content from geojson file
    test_file_path = os.path.join(THIS_DIR, 'test_data.geojson')
    with open(test_file_path) as geojson_file:
        content = json.load(geojson_file)
        data = {
                'r1': "",
                'r2': "",
                "dated_profiles": [
                    {
                        'date': "2020-01-01",
                        'uncertainty': 20,
                        'geojson': content,
                    }
                ]
        }

    client = api_client()
    url = reverse("api-v1-dune:datedprofile-list", args=[transect.uuid])
    response = client.post(url, data)

    assert response.status_code == status.HTTP_202_ACCEPTED
    assert response.data["status"] == "PENDING"
    assert response.data["task_id"] != ""

    flower_response = client.get(reverse("api-v1-common:flower-task-list"))
    assert flower_response.status_code == status.HTTP_200_OK
    assert len(flower_response.data) != 0
    assert response.data["task_id"] in flower_response.data.keys()
