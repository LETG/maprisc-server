import pytest
import os
import pandas as pd
from shapely.geometry import Point


from maprisc_backend.apps.dune.models import Transect, DatedProfile
from maprisc_backend.apps.dune.utils.dune import *
from maprisc_backend.apps.dune.utils.preprocessing import *
from maprisc_backend.apps.dune.api.v1.tasks import check_input_profile, get_reference_distance_from_transect_head

THIS_DIR = os.path.dirname(os.path.abspath(__file__))

@pytest.fixture(scope='module')
def dgps_data():
    '''
    Using OMDM profile n° 12 with date 18-04-19
    '''
    profile_features = {
        "type" : "FeatureCollection",
        "features" : [
            {
                "type" : "Feature",
                "id" : 1,
                "geometry" : {
                    "type" : "Point",
                    "coordinates" : [308656.72, 6647782.551, 6.098]
                },
                "properties": {}
            },
            {
                "type" : "Feature",
                "id" : 1,
                "geometry" : {
                    "type" : "Point",
                    "coordinates" : [308650.835, 6647777.121, 6.133]
                },
                "properties": {}
            },
            {
                "type" : "Feature",
                "id" : 1,
                "geometry" : {
                    "type" : "Point",
                    "coordinates" : [308646.095, 6647772.832, 5.9]
                },
                "properties": {}
            },
            {
                "type" : "Feature",
                "id" : 1,
                "geometry" : {
                    "type" : "Point",
                    "coordinates" : [308640.853, 6647768.115, 7.01]
                },
                "properties": {
                    "code": "CDD"
                }
            },
            {
                "type" : "Feature",
                "id" : 1,
                "geometry" : {
                    "type" : "Point",
                    "coordinates" : [308628.502, 6647756.77, 4.132]
                },
                "properties": {
                    "code": "PDD"
                }
            },
            {
                "type" : "Feature",
                "id" : 1,
                "geometry" : {
                    "type" : "Point",
                    "coordinates" : [308609.62, 6647739.24, 1.896]
                },
                "properties": {}
            }
        ]
    }
    profile_axis = ((308798.046599999, 6647912.7339), (308017.648, 6647194.8254))
    profile_head = (308662.7591, 6647788.279)

    return {
            'profile_features': profile_features,
            'profile_head': profile_head,
            'profile_axis': profile_axis
        }

@pytest.fixture(scope='module')
def input_data():
    profile_points = [
        (0,0), (1,1), (2,2), (3,3)
    ]
    profile_axis = ((0, 0), (3, 3))
    profile_head = (4,4)

    return {
            'profile': profile_points,
            'profile_head': profile_head,
            'profile_axis': profile_axis
        }

def test_basic_projection(input_data):
    selected_point = Point(input_data['profile'][2])
    projected_point = project_point_along_line(selected_point, input_data['profile_axis'])
    assert type(projected_point) == Point
    assert projected_point.x == 2
    assert projected_point.y == 2

def test_real_projection(dgps_data):
    selected_point = Point(dgps_data['profile_features']['features'][0]['geometry']['coordinates'])
    projected_point = project_point_along_line(selected_point, dgps_data['profile_axis'])
    assert type(projected_point) == Point
    assert projected_point.x == 308656.72
    assert projected_point.y == 6647782.551

def test_distance_to_profile_head(input_data):
    selected_point = Point(input_data['profile'][2])
    distance = compute_distance_to_profile_head(selected_point, input_data['profile_head'])
    assert distance == round(math.sqrt(8), 3)

def test_real_distance(dgps_data):
    selected_point = Point(dgps_data['profile_features']['features'][0]['geometry']['coordinates'])
    distance = compute_distance_to_profile_head(selected_point, dgps_data['profile_head'])
    assert distance == 8.324

def test_preprocessing(dgps_data):
    input_profile = {
        'date': '18-04-2019',
        'uncertainty': 5,
        'geojson': dgps_data['profile_features']
    }
    updated_data = update_geojson_profile_with_distance(input_profile, dgps_data['profile_head'], dgps_data['profile_axis'])
    assert type(updated_data) == dict