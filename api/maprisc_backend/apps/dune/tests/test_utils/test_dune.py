from django.urls import reverse
from django.contrib.gis.geos import Point, MultiPoint

from rest_framework import status
from rest_framework.response import Response

import pytest
import os
import json
import pandas as pd
import numpy as np
from collections import OrderedDict

from maprisc_backend.apps.dune.models import Transect, DatedProfile
from maprisc_backend.apps.dune.utils.dune import *
from maprisc_backend.apps.dune.api.v1.tasks import check_input_profile, get_reference_distance_from_transect_head

THIS_DIR = os.path.dirname(os.path.abspath(__file__))

@pytest.fixture(scope='module')
def default_processing_params():
    params = {
        'lowpass_foot': [0.11, 0.22, 0.34, 0.22, 0.11],
        'lowpass_dgps': [0.25, 0.50, 0.25],
        'highpass': [-1, 0, 1],
        'second_highpass': [1, 0, -1],
        'z_th': 3,
        'hollow_quantile': 0.85,
    }
    return params

@pytest.fixture(scope='module')
def simple_data():
    # Create a simple f(x)=x curve
    points_z = [0,1,2,3,4]
    points_x = [0,1,2,3,4]

    return pd.DataFrame(
        {
            'point_index': points_x,
            'z': points_z
        }
    )

@pytest.fixture(scope='module')
def dgps_data():
    points_z = [0,0.256,1.127,2.016,3.1,4.665,5.876,6.378,7.751,7.584,7.981]
    points_x = [88.1256186998247,95.927379314357,118.349228027733,137.479050751407,158.822858157294,
            173.45319789553,178.548084665155,179.443147946197,186.382357298458,187.085291553463,190.772551853633]

    return pd.DataFrame(
        {
            'point_index': points_x,
            'z': points_z
        }
    )

def test_dgps_needs_resampling(dgps_data):
    # Fixture DGPS profile ratio is about 1/10 so needs resampling
    assert profile_needs_resampling(dgps_data) == True


def test_dgps_profile_processing(dgps_data, default_processing_params):
    resampling = True
    output = profiles_processing(default_processing_params, dgps_data, resampling)
    # Profile will be resampled so assert dimension has changed
    assert output.shape == (112, 12)

def test_dgps_reverse_profile_processing():

    # Create dict content from geojson file
    test_file_path = os.path.join(THIS_DIR, 'test_reverse_data.geojson')
    with open(test_file_path) as geojson_file:
        content = json.load(geojson_file)
        data = {
            'date': "2018-04-01",
            'geojson': content,
        }  # nosec

    reference_distance = get_reference_distance_from_transect_head([data])
    output = check_input_profile(data, reference_distance)
    dated_profile = output['profile']
    # Assert reference distance is correct
    assert reference_distance == 252.6162274
    first_point_coords = dated_profile["geojson"]["features"][0]["geometry"]["coordinates"]
    last_point_coords = dated_profile["geojson"]["features"][-1]["geometry"]["coordinates"]
    # Assert altitude of first index is lower than altitude of last index
    assert first_point_coords[2] < last_point_coords[2]
    # Assert profile has been correctly reversed: last feature is now first
    last_original_point = data['geojson']['features'][-1]
    assert last_original_point['geometry']['coordinates'] == last_point_coords
    # Assert length of profile is still the same
    assert len(dated_profile['geojson']['features']) == len(data['geojson']['features'])

def test_scipy_volume(simple_data):
    profile = simple_data['z']
    x_vector = simple_data['point_index'].to_list()
    spacing_unit = 1
    method = "simps"
    volume = compute_volume(profile, x_vector, spacing_unit=spacing_unit, method=method)

    assert volume == 8

def test_linear_interpolation(simple_data):
    point_list = simple_data.to_records(index=False).tolist()
    index_reference = 2.5
    result = linear_interpolation_1d(point_list, index_reference)

    assert result == 2.5

def test_volume_computation(dgps_data, default_processing_params):
    # First, compute dgps profile
    resampling = True
    profile = profiles_processing(default_processing_params, dgps_data, resampling)

    # Use existing points in profile so interpolation is not used
    min_dune_toe_ref = 179
    window_low_ref = 88.1256186998247
    window_high_ref = 190.772551853633
    profile_points = {
        'points_position': [],
        'uncertainty': 5,
        'volumes': []
    }
    # Transform profile dataframe to list of (index,z) points (i.e. tuples)
    profile_points['points_position'] = profile[['point_index', 'z']].to_records(index=False).tolist()
    result = compute_profile_volumes(profile_points, min_dune_toe_ref, window_low_ref, window_high_ref)
    volumes = result['volumes']
    shore_stage_volume = next(x for x in volumes if x["min_ref"] == 'Lower point' and x["max_ref"] == 'Dune Foot')['volume']
    dune_stage_volume = next(x for x in volumes if x["min_ref"] == 'Dune Foot' and x["max_ref"] == 'Dune Crest')['volume']
    global_stage_volume = next(x for x in volumes if x["min_ref"] == 'Lower point' and x["max_ref"] == 'Dune Crest')['volume']
    
    assert int(shore_stage_volume) == 187
    assert int(dune_stage_volume) == 85
    assert int(global_stage_volume) == 273

def test_volume_computation_with_inconsistent_boundary(dgps_data, default_processing_params):
    # Test if volumes are correctly computed when using a boundary (point index) from another profile that does not exist in the data
    # This occurs when processing discrete 1D profiles (DGPS) for which lowest dune toe index has a specific position (not created by resampling)
    # In these cases, boundary altitude should be interpolated to compute consistent volumes among windows.
    # WARNING : final result still slightly differs due to interpolation. TODO: Measure precision
    resampling = True
    profile = profiles_processing(default_processing_params, dgps_data, resampling)

    # Use point indices that do not exists in profile so interpolation is used for the 3 boundaries
    different_profile_min_dune_toe_ref = 178.22
    window_low_ref = 89.15
    window_high_ref = 190

    profile_points = {
        'points_position': [],
        'uncertainty': 5,
        'volumes': []
    }
    # Transform profile dataframe to list of (index,z) points (i.e. tuples)
    profile_points['points_position'] = profile[['point_index', 'z']].to_records(index=False).tolist()
    result = compute_profile_volumes(profile_points, different_profile_min_dune_toe_ref, window_low_ref, window_high_ref)
    volumes = result['volumes']
    shore_stage_volume = next(x for x in volumes if x["min_ref"] == 'Lower point' and x["max_ref"] == 'Dune Foot')['volume']
    dune_stage_volume = next(x for x in volumes if x["min_ref"] == 'Dune Foot' and x["max_ref"] == 'Dune Crest')['volume']
    global_stage_volume = next(x for x in volumes if x["min_ref"] == 'Lower point' and x["max_ref"] == 'Dune Crest')['volume']
    
    assert int(shore_stage_volume) == 182
    assert int(dune_stage_volume) == 84
    assert int(global_stage_volume) == 267

    # Assert difference between global volume and sum of both stages volumes is less than 1m3 (points are separated from less than 1m)
    assert (global_stage_volume - (dune_stage_volume + shore_stage_volume)) < 1
    # Assert new points have been added to points_position
    assert different_profile_min_dune_toe_ref in [point[0] for point in result['points_position']]
    assert window_low_ref in [point[0] for point in result['points_position']]
    assert window_high_ref in [point[0] for point in result['points_position']]

def test_volume_uncertainty(dgps_data):
    # Compute uncertainty
    point_position_uncertainty = 0.03 #meters
    volume_uncertainty = compute_profile_volume_uncertainty(dgps_data, point_position_uncertainty)

    assert round(volume_uncertainty, 2) == 0.98
    assert len(dgps_data.index) % 2 == 1
    assert (len(dgps_data.index) -1) // 2 == 5
