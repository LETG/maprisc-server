from django.contrib.gis.db import models as gis_models
from django.contrib.postgres.fields import ArrayField

from maprisc_backend.apps.common import models as core_models
from maprisc_backend.apps.common.models import CoreModel
from maprisc_backend.apps.accounts.models import UserAccount

class Transect(CoreModel):
    index = gis_models.FloatField()
    name = gis_models.CharField(max_length=100, blank=True, null=True)
    autosearch = gis_models.BooleanField(blank=True, null=True)
    phma = gis_models.FloatField(blank=True, null=True) # Mandatory if autosearch mode is set to True
    profile_head = gis_models.PointField(blank=True, null=True, srid=2154)
    profile_axis = gis_models.LineStringField(blank=True, null=True, srid=2154)
    distance_to_profile_head = gis_models.FloatField(blank=True, null=True) # Specific field to keep reference for 1D dated profile alignment (DGPS distance)
    user = gis_models.ForeignKey(
        UserAccount,
        on_delete=gis_models.CASCADE,
        related_name='transects',
        blank=True,
        null=True,
    )

class DatedProfile(CoreModel):
    date = gis_models.DateTimeField()
    crest_index = gis_models.FloatField()
    foot_index = gis_models.FloatField()
    uncertainty = gis_models.FloatField(blank=True, null=True, default=0)
    autosearch = gis_models.BooleanField(blank=True, null=True)
    # TODO : ajouter l'écart entre points !
    geom = gis_models.MultiPointField(blank=True, null=True, dim=3, srid=2154) # dim is mandatory to handle z dimension
    points_position = ArrayField(ArrayField(gis_models.FloatField()), blank=True, null=True)
    transect = gis_models.ForeignKey(
        'Transect',
        on_delete=gis_models.CASCADE,
        related_name='dated_profiles',
        blank=True,
        null=True,
    )

class DatedProfileVolume(CoreModel):
    min_ref = gis_models.CharField(max_length=24)
    min_index = gis_models.FloatField(blank=True, null=True)
    max_ref = gis_models.CharField(max_length=24)
    max_index = gis_models.FloatField(blank=True, null=True)
    volume = gis_models.FloatField(blank=True, null=True)
    uncertainty = gis_models.FloatField(blank=True, null=True)
    dated_profile = gis_models.ForeignKey(
        'DatedProfile',
        on_delete=gis_models.CASCADE,
        related_name='volumes',
        blank=True,
        null=True
    )
