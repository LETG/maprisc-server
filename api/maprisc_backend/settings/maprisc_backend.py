from datetime import timedelta
import logging.config

from .environment import env


MAPRISC_BACKEND_FEATURES = env.list("MAPRISC_BACKEND_FEATURES", default=[])

MAPRISC_BACKEND_EMAIL_FROM = env.str("MAPRISC_BACKEND_EMAIL_FROM", default="no-reply@example.com")

MAPRISC_BACKEND_AUTH_COOKIE_NAME = env.str("MAPRISC_BACKEND_AUTH_COOKIE_NAME", default="a")

# Reset password link lifetime interval (in seconds). By default: 1 hour.
MAPRISC_BACKEND_RESET_PASSWORD_EXPIRATION_DELTA = timedelta(seconds=env.int("MAPRISC_BACKEND_RESET_PASSWORD_EXPIRATION_DELTA", default=3600))

# Logging Configuration

# Clear prev config
LOGGING_CONFIG = None

# Get loglevel from env
LOGLEVEL = env.str('MAPRISC_BACKEND_LOGLEVEL', default='info').upper()

logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {
            'format': '%(asctime)s %(levelname)s [%(name)s:%(lineno)s] %(module)s %(process)d %(thread)d %(message)s',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'console',
        },
    },
    'loggers': {
        '': {
            'level': LOGLEVEL,
            'handlers': ['console',],
        },
    },
})

if "LOG_SQL" in MAPRISC_BACKEND_FEATURES:  # pragma: no cover
    LOGGING = {
        "version": 1,
        "disable_existing_loggers": False,
        "filters": {"require_debug_true": {"()": "django.utils.log.RequireDebugTrue"}},
        "formatters": {
            "simple": {"format": "[%(asctime)s] %(levelname)s %(message)s"},
            "verbose": {"format": "[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s"},
            "sql": {"()": "maprisc_backend.loggers.SQLFormatter", "format": "[%(duration).3f] %(statement)s"},
        },
        "handlers": {
            "console": {"class": "logging.StreamHandler", "formatter": "verbose", "level": "DEBUG"},
            "sql": {"class": "logging.StreamHandler", "formatter": "sql", "level": "DEBUG"},
        },
        "loggers": {
            "django.db.backends": {
                "handlers": ["sql"],
                "level": "DEBUG",
                "filters": ["require_debug_true"],
                "propagate": False,
            },
            "django.db.backends.schema": {"handlers": ["console"], "level": "DEBUG", "propagate": False},
        },
    }