from ..environment import env


SWAGGER_SETTINGS = {
    "DEFAULT_API_URL": env.str("MAPRISC_BACKEND_BASE_API_URL", default="https://example.com"),
    "SECURITY_DEFINITIONS": {"JWT": {"type": "apiKey", "name": "JWT", "in": "header"}},
}
