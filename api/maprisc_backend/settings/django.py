import os
from datetime import timedelta

from corsheaders.defaults import default_headers

from .environment import env

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

sentry_sdk.init(
    dsn=env.str("MAPRISC_BACKEND_SENTRY_DSN"),
    integrations=[DjangoIntegration()],

    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True
)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def rel(*path):
    return os.path.join(BASE_DIR, *path)


DEBUG = env.bool("MAPRISC_BACKEND_DEBUG", default=False)

INTERNAL_IPS = env.list("MAPRISC_BACKEND_INTERNAL_IPS", default=[])

ALLOWED_HOSTS = env.list("MAPRISC_BACKEND_ALLOWED_HOSTS", default=[])

SECRET_KEY = env.str("MAPRISC_BACKEND_SECRET_KEY")

INSTALLED_APPS = [
    # django apps
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.gis",
    # 3rd party apps
    "rest_framework",
    'rest_framework_gis',
    "django_extensions",
    "django_filters",
    "drf_yasg",
    "djgeojson",
    'corsheaders',
    "django_rest_passwordreset",
    # our apps
    "maprisc_backend.apps.common.apps.CommonConfig",
    "maprisc_backend.apps.accounts.apps.AccountConfig",
    "maprisc_backend.apps.dune.apps.DuneConfig",
    "maprisc_backend.apps.boxes.apps.BoxesConfig",
] + env.list("MAPRISC_BACKEND_DEV_INSTALLED_APPS", default=[])

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    'corsheaders.middleware.CorsMiddleware',
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
] + env.list("MAPRISC_BACKEND_DEV_MIDDLEWARE", default=[])

ROOT_URLCONF = "maprisc_backend.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [rel("templates/")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

WSGI_APPLICATION = "maprisc_backend.wsgi.application"

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': env.str("MAPRISC_BACKEND_DATABASE_NAME"),
        'USER': env.str("MAPRISC_BACKEND_DATABASE_USER"),
        'PASSWORD': env.str("MAPRISC_BACKEND_DATABASE_PASSWORD"),
        'HOST': env.str("MAPRISC_BACKEND_DATABASE_HOST"),
        'PORT': env.str("MAPRISC_BACKEND_DATABASE_PORT"),
    }
}

SERIALIZATION_MODULES = {
    'geojson': 'djgeojson.serializers',
}

AUTH_USER_MODEL = "accounts.UserAccount"
AUTH_PASSWORD_VALIDATORS = [
    {"NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"},
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

SECURE_BROWSER_XSS_FILTER = env.bool("MAPRISC_BACKEND_SECURE_BROWSER_XSS_FILTER", default=True)
SECURE_CONTENT_TYPE_NOSNIFF = env.bool("MAPRISC_BACKEND_SECURE_CONTENT_TYPE_NOSNIFF", default=True)
SESSION_COOKIE_HTTPONLY = env.bool("MAPRISC_BACKEND_SESSION_COOKIE_HTTPONLY", default=True)
SESSION_COOKIE_SECURE = env.bool("MAPRISC_BACKEND_SESSION_COOKIE_SECURE", default=True)
CSRF_COOKIE_SECURE = env.bool("MAPRISC_BACKEND_CSRF_COOKIE_SECURE", default=True)
X_FRAME_OPTIONS = env.str("MAPRISC_BACKEND_X_FRAME_OPTIONS", default="SAMEORIGIN")
SECURE_HSTS_SECONDS = env.int("MAPRISC_BACKEND_SECURE_HSTS_SECONDS", default=31536000)  # 1 year
SESSION_COOKIE_NAME = "s"
CSRF_COOKIE_NAME = "c"

LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True
LOCALE_PATHS = (rel("..", "..", "api", "locale"),)

STATIC_URL = env.str("MAPRISC_BACKEND_STATIC_URL", default="/s/")
STATIC_ROOT = env.str("MAPRISC_BACKEND_STATIC_ROOT", default=rel("..", "..", "public", "static"))

MEDIA_URL = env.str("MAPRISC_BACKEND_MEDIA_URL", default="/m/")
MEDIA_ROOT = env.str("MAPRISC_BACKEND_MEDIA_ROOT", rel("..", "..", "public", "media"))
FILE_UPLOAD_PERMISSIONS = 0o644

EMAIL_BACKEND = env.str("MAPRISC_BACKEND_EMAIL_BACKEND", default="django.core.mail.backends.smtp.EmailBackend")
if EMAIL_BACKEND == "django.core.mail.backends.smtp.EmailBackend":  # pragma: no cover
    EMAIL_HOST = env.str("MAPRISC_BACKEND_EMAIL_HOST")
    EMAIL_PORT = env.str("MAPRISC_BACKEND_EMAIL_PORT")
    EMAIL_HOST_USER = env.str("MAPRISC_BACKEND_EMAIL_HOST_USER")
    EMAIL_HOST_PASSWORD = env.str("MAPRISC_BACKEND_EMAIL_HOST_PASSWORD")
    EMAIL_USE_TLS = env.bool("MAPRISC_BACKEND_EMAIL_USE_TLS", default=True)

SITE_ID = env.int("SITE_ID", default=1)

MAPRISC_FRONTEND_URL = env.str("MAPRISC_FRONTEND_URL")

USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

APPEND_SLASH = False
CELERY_BACKEND = env.str("MAPRISC_BACKEND_CELERY_BACKEND")

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    )
}

# CORS configuration using django-cors-header middleware
from corsheaders.defaults import default_headers

CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = env.list("MAPRISC_BACKEND_CORS_WHITELIST", default=[])
CORS_ALLOW_HEADERS = list(default_headers) + [
    "content-encoding",
    "baggage",
    "sentry-trace"
]

# JWT configuration
SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=180),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=2),
    'ROTATE_REFRESH_TOKENS': False,
    'BLACKLIST_AFTER_ROTATION': True,

    'ALGORITHM': 'HS256',
    'SIGNING_KEY': env.str("MAPRISC_BACKEND_SECRET_KEY"),
    'VERIFYING_KEY': None,
    'AUDIENCE': None,
    'ISSUER': None,

    'AUTH_HEADER_TYPES': ('Bearer',),
    'USER_ID_FIELD': 'uuid',
    'USER_ID_CLAIM': 'user_id',

    'AUTH_TOKEN_CLASSES': ('rest_framework_simplejwt.tokens.AccessToken',),
    'TOKEN_TYPE_CLAIM': 'token_type',

    'JTI_CLAIM': 'jti',

    'SLIDING_TOKEN_REFRESH_EXP_CLAIM': 'refresh_exp',
    'SLIDING_TOKEN_LIFETIME': timedelta(minutes=5),
    'SLIDING_TOKEN_REFRESH_LIFETIME': timedelta(days=1),
}

# Django REST Reset Password config
DJANGO_REST_MULTITOKENAUTH_RESET_TOKEN_EXPIRY_TIME=env.float("DJANGO_REST_MULTITOKENAUTH_RESET_TOKEN_EXPIRY_TIME")
DJANGO_REST_PASSWORDRESET_NO_INFORMATION_LEAKAGE=env.bool("DJANGO_REST_PASSWORDRESET_NO_INFORMATION_LEAKAGE")
DJANGO_REST_MULTITOKENAUTH_REQUIRE_USABLE_PASSWORD=env.bool("DJANGO_REST_PASSWORDRESET_NO_INFORMATION_LEAKAGE")