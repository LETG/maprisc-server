import pytest

from model_bakery import baker

from maprisc_backend.apps.accounts.models.user_account import UserAccount


__all__ = ["user_account", "fake_user"]


@pytest.fixture()
def user_account(db):  # pylint: disable=unused-argument
    def _user(**kwargs):
        return baker.make("accounts.UserAccount", **kwargs)

    return _user

@pytest.fixture()
def fake_user(db):
    data = {"email": "jane@example.com", "password": "super-secret-password"}  # nosec
    user = UserAccount.objects.create_user(data["email"], data["password"])

    return user