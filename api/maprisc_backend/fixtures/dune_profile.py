import pytest

from model_bakery import baker


__all__ = ["dune_transect", "dune_datedprofile"]


@pytest.fixture()
def dune_transect(db):  # pylint: disable=unused-argument
    def _transect(**kwargs):
        return baker.make("dune.Transect", **kwargs)

    return _transect

@pytest.fixture()
def dune_datedprofile(db):  # pylint: disable=unused-argument
    def _dated_profile(**kwargs):
        return baker.make("dune.DatedProfile", **kwargs)

    return _dated_profile