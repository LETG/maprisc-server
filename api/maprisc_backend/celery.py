import os
from celery import Celery
from celery.app.task import Task
import logging

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "maprisc_backend.settings")

logger=logging.getLogger(__name__)

class LoggingTask(Task):
    def on_failure(self, exc, task_id, args, kwargs, einfo):
        kwargs={}
        if logger.isEnabledFor(logging.DEBUG):
            kwargs['exc_info']=exc
        logger.error('Task %f failed to execute', task_id, **kwargs)
        super().on_failure(exc, task_id, args, kwargs, einfo)

from django.conf import settings  # noqa

celery_app = Celery("maprisc_backend")
celery_app.config_from_object("django.conf:settings", namespace="CELERY")
celery_app.Task = LoggingTask
celery_app.autodiscover_tasks()
