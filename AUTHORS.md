Primary Authors
===============

* __[Benjamin Hervy](https://orcid.org/0000-0002-5755-6478)__
* __[Martin Juigner](https://www.univ-nantes.fr/martin-juigner)__
* __[Cyril Pulvin](https://igarun.univ-nantes.fr/cyril-pulvin)__